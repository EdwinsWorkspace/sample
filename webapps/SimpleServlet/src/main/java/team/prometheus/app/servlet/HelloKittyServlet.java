package team.prometheus.app.servlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created on 2016/5/6
 * Simple Servlet prints Hello Kitty.
 *
 * @author zhangshuai
 * @version 1.0.0
 * @since 1.0.0
 */
public class HelloKittyServlet extends HttpServlet {
    @Override protected void doGet(HttpServletRequest req, HttpServletResponse resp)
        throws ServletException, IOException {

        resp.getWriter().print("Hello Kitty !!!");
    }
}
