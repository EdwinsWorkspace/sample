package team.prometheus.app.sample.utils;

/**
 * Created on 2016/5/19
 * [Function]
 *
 * @author zhangshuai
 * @version 1.0.0
 * @since 1.0.0
 */
public class Printer {

    /**
     * default repeat times
     */
    private final static int REPEAT_NUM = 50;

    /**
     * Hide private method
     */
    private Printer(){}

    /**
     * TODO: need to support (array, string)
     * @param params
     */
    public static synchronized <T> void println(T... params) {
        for (Object p : params) {
            System.out.print(p);
        }
        System.out.println();
    }

    public  static synchronized <T> void printWithClassName(T param) {
        StringBuffer sbf = new StringBuffer();
        sbf.append(param.getClass().getName()).append(" :: ").append(param);

        println(">>[", sbf.toString(), "]<<");
    }

    public static synchronized void repeatedPrint(String original){
        repeatedPrint(original,REPEAT_NUM);
    }

    public static synchronized void repeatedPrint(String original, int num){
        println(repeat(original,num));
    }

    public static synchronized String repeat(String original, int num) {
        return new String(new char[num]).replace("\0",original);
    }
}
