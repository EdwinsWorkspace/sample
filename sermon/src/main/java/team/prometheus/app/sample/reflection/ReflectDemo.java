package team.prometheus.app.sample.reflection;

import lombok.extern.slf4j.Slf4j;

/**
 * Created on 2016/6/12
 * [Function]
 *
 * @author zhangshuai
 * @version 1.0.0
 * @since 1.0.0
 */
@Slf4j
public class ReflectDemo {

    public static void main(String[] args)
        throws ClassNotFoundException, IllegalAccessException, InstantiationException {

        // 1.1 fornName(..)
        // Class clazzz = Class.forName("team.prometheus.app.sample.reflection.CustomFoo");

        // 1.2 forName(..), you can skip static initialization like this
        Class clazz = Class.forName("team.prometheus.app.sample.reflection.CustomFoo", false,
            Thread.currentThread().getContextClassLoader());
        log.info(">>End<< class load");

        // 2. use Class directly
        Class clazz2 = CustomFoo.class;

        // >>>> declaration won't load this class until you init it.
        CustomFoo foo3;
        log.info(">>End<< declaration");
        foo3 = new CustomFoo();
        log.info(">>End<< assign");

        // 3. use a instance
        Class clazz3 = foo3.getClass();

        // >>>> It won't initial static block again
        CustomFoo foo1 = (CustomFoo) clazz.newInstance();
        CustomFoo foo2 = (CustomFoo) clazz2.newInstance();

    }

}
