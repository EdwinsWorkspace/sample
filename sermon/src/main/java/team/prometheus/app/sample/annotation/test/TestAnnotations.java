package team.prometheus.app.sample.annotation.test;

import team.prometheus.app.sample.annotation.Test;
import team.prometheus.app.sample.annotation.TesterInfo;
import team.prometheus.app.sample.annotation.TesterInfo.Priority;

/**
 * Created on 2016/5/20
 * [Function]
 *
 * @author zhangshuai
 * @version 1.0.0
 * @since 1.0.0
 */
@TesterInfo(
    priority = Priority.HIGH,
    createdBy = "shuai.zhang",
    tags = {"test", "annotations"}
)
public class TestAnnotations {

    @Test(enabled = false)
    public void testA() {
        if (Boolean.FALSE) {
            throw new RuntimeException("This test always failed");
        }
    }

    @Test(enabled = false)
    public void testB() {
        if (Boolean.FALSE) {
            throw new RuntimeException("This test always passed");
        }
    }

    @Test(enabled = true)
    public void testC() {
        if (10 > 1) {
            // do nothing, this test always passed.
        }
    }
}
