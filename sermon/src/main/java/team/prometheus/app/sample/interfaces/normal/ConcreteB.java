package team.prometheus.app.sample.interfaces.normal;

import team.prometheus.app.sample.utils.Printer;

/**
 * Created on 2016/6/2
 * [Function]
 *
 * @author zhangshuai
 * @version 1.0.0
 * @since 1.0.0
 */
public class ConcreteB extends AbstractEntity {

    @Override public void operations() {
        abstractMethod();
        Printer.println("Eating...");
        Printer.println("End");
    }
}
