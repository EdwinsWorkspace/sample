package team.prometheus.app.sample.sort.merge;

import java.util.Arrays;

/**
 * Created on 16/6/30
 * [Function]
 * 用于演示简单的Merge排序
 *
 * @author edwin
 * @version 1.2.0
 * @since 1.1.0
 */
public class MergeSort {

    public static void main(String[] args) {

        int[] a = {1,3,5,7,8,9};
        int[] b = {2,3,4,14,56,78,89,100};

//        int[] a = {};
//        int[] b = {};

        int[] ret = mergeSort(a, b);

        System.out.println(Arrays.toString(ret));
    }

    /**
     * Merge 排序方法
     *
     * 右后向前依次移动比较两个数组元素放入返回数组中
     * 当发现其中任意源数组中遍历结束时, 中断循环, 并将剩余元素循环放入返回数组
     *
     * @param a 源数组a
     * @param b 源数组b
     * @return 排序结果
     */
    private static int[] mergeSort(int[] a, int[] b) {

        int[] ret = new int[a.length + b.length];

        if (a.length == 0 && b.length == 0) return ret;

        int ai = a.length - 1;
        int bi = b.length - 1;
        int ri = a.length + b.length - 1;

        while (ri >= 0) {
            if (ai < 0 || bi < 0) break; // 跳出循环

            if (a[ai] > b[bi]) {
                ret[ri] = a[ai];
                ai--;
            } else {
                ret[ri] = b[bi];
                bi--;
            }
            ri--;
        }

        // 将两个数组中剩余元素循环放到返回数组里面
        while (bi >= 0) {
            ret[ri] = b[bi];
            bi--;
            ri--;
        }
        while (ai >= 0) {
            ret[ri] = a[ai];
            ai--;
            ri--;
        }

        return ret;
    }
}
