package team.prometheus.app.sample.generics;

/**
 * Created on 16/6/11
 * [Function]
 *
 * @author edwin
 * @version 1.1.0
 * @since 1.1.0
 */

//@Getter @Setter
public class GenericFoo<T> {

    private T entity;

    public T getEntity(){
        return entity;
    }

    public void setEntity(T entity) {
        this.entity = entity;
    }
}
