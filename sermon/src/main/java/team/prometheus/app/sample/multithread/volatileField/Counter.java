package team.prometheus.app.sample.multithread.volatileField;

import lombok.extern.slf4j.Slf4j;

/**
 * Created on 16/6/23
 * [Function]
 *
 * @author edwin
 * @version 1.1.0
 * @since 1.1.0
 */
@Slf4j
public class Counter {

    private volatile Status status = Status.STARTED;

    public void start() {
        new Thread( () -> {

            while (Status.STARTED.equals(status)) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    System.out.print(".");
                }
            }
        }).start();
    }

    public void stop(){
        this.status = Status.STOPPED;
    }

    private enum Status {
        STARTED,
        STOPPED
    }

    public static void main(String[] args) {
        Counter s = new Counter();

        s.start();

        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            s.stop();
        }
    }
}


