package team.prometheus.app.sample.multithread.thread;

import team.prometheus.app.sample.utils.Printer;

/**
 * Created on 16/6/3
 * [Function]
 *
 * @author edwin
 * @version 1.1.0
 * @since 1.1.0
 */
public class MyThread extends Thread{

    int count = 1;
    int id;

    public MyThread(int id){
        this.id = id;
        Printer.println("## my thread ", id, " is created...##");
    };

    @Override
    public void run() {
        while (true) {
            // count 1 to 100
            Printer.println(">> thread id: ", id, ", counting: ", count++, "<<" );
            if (count > 100) break;
        }
    }
}
