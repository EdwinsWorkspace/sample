package team.prometheus.app.sample.multithread.producer;

import team.prometheus.app.sample.utils.Printer;

/**
 * Created on 16/6/7
 * [Function]
 *
 * @author edwin
 * @version 1.1.0
 * @since 1.1.0
 */
public class Consumer implements Runnable{

    Queue queue;

    public Consumer(Queue queue) {
        this.queue = queue;
    }

    @Override
    public void run() {
        Printer.println("checking...");
        while (true) {
            if (queue == null || !queue.queue.isEmpty()) {
                try {
                    Printer.println("Consumer waiting...");
                    Thread.sleep(100);
                    Printer.println("Consumer awake...");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            for (Integer key : queue.queue.keySet()) {
                Printer.println("Consumed product:", key);
                queue.queue.remove(key);
            }
        }
    }
}
