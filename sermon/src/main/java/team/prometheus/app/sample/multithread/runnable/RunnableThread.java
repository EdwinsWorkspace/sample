package team.prometheus.app.sample.multithread.runnable;

import team.prometheus.app.sample.utils.Printer;

/**
 * Created on 16/6/6
 * [Function]
 *
 * @author edwin
 * @version 1.1.0
 * @since 1.1.0
 */
public class RunnableThread implements Runnable {

    int count = 1, id;

    public RunnableThread (int id) {
        this.id = id;
    }

    @Override
    public void run() {
        while (true) {
            // count 1 to 100
            Printer.println(">> thread id: ", id, ", counting: ", count++, "<<" );
            if (count > 100) return;
        }

    }
}
