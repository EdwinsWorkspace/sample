package team.prometheus.app.sample.collctions.list.linkedlist;

import java.util.LinkedList;

/**
 * Created on 16/6/11
 * [Function]
 *
 * @author edwin
 * @version 1.1.0
 * @since 1.1.0
 */
public class LinkedListDemo {

    public static void main (String[] args){
        LinkedList<String> list = new LinkedList<>();

        list.addFirst("Hi,");
        list.addLast("World!");

        list.forEach((t)-> System.out.print(t));

    }
}
