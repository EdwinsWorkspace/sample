package team.prometheus.app.sample.collctions.list.arraylist;

import lombok.extern.slf4j.Slf4j;
import team.prometheus.app.sample.utils.Printer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 2016/5/17
 * [Function]
 * -----------------------------------
 * Tests index changes if we removed items from a lst
 * -----------------------------------
 *
 * [print result]
 * -----------------------------------
 * index:0; value:4
 * index:1; value:3
 * index:2; value:2
 * index:3; value:1
 * -----------------------------------
 * index:0; value:4
 * index:1; value:3
 * index:2; value:1
 * -----------------------------------
 *
 * @author zhangshuai
 * @version 1.0.0
 * @since 1.0.0
 */
@Slf4j
public class ArrayListIndexTest {

    public static void main (String [] args) {

        List<String> t = new ArrayList<>(10);
        for (int i =4; i>0; i--) {
            t.add(String.valueOf(i));
        }

        for (String s : t) {
            System.out.println("index:" + t.indexOf(s) + "; value:" + s);
        }
        // removed t[2]
        t.remove(2);
        log.warn(Printer.repeat("#", 20));

        for (String s : t) {
            System.out.println("index:" + t.indexOf(s) + "; value:" + s);
        }
    }
}

