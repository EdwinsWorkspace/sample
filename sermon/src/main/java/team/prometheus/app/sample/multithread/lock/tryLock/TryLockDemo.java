package team.prometheus.app.sample.multithread.lock.tryLock;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import team.prometheus.app.sample.utils.Printer;

/**
 * Created on 2016/6/7
 * [Function]
 *
 * @author zhangshuai
 * @version 1.0.0
 * @since 1.0.0
 */
public class TryLockDemo {

    static int i = 1;
    static Lock lock = new ReentrantLock();

    public static void main(String[] args){

        new Thread(new Runnable() {
            @Override public void run() {
                lock.lock();
                try {
                    for (int k=0; k<10; k++) {
                        Printer.println(">>1<<", i++);
                    }
                } finally {
//                    lock.unlock();
                }
            }
        }).start();

        new Thread(new Runnable() {
            @Override public void run() {
                if (lock.tryLock()) {
                    try {
                        for (int k=0; k<10; k++) {
                            Printer.println(">>2<<", i++);
                        }
                    } finally {
                        lock.unlock();
                    }
                } else {
                    Printer.println("Can't get the lock...");
                }
            }
        }).start();
    }
}
