package team.prometheus.app.sample.jdbc;

import com.google.common.base.Preconditions;
import com.mysql.jdbc.Statement;
import team.prometheus.app.sample.utils.Printer;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created on 16/6/11
 * [Function]
 *
 * @author edwin
 * @version 1.1.0
 * @since 1.1.0
 */
public class SimpleJdbcDemo {

    /**
     * -----------------------------------------------------------------
     * TODO: Please move this to a property file, and try to use Property util
     * -----------------------------------------------------------------
     */
    private final static String URL = "jdbc:mysql://localhost:3306/post";
    private final static String USER_NAME = "test";
    private final static String PASSWORD = "123456";
    private final static String SELECT_SQL = "SELECT * FROM post;";

    public static void main(String[] args) throws ClassNotFoundException {

        // load driver
        Class.forName("com.mysql.jdbc.Driver");

        StringBuilder sb = new StringBuilder();
        sb = sb.append(URL).append("?")
                .append("user=").append(USER_NAME).append("&")
                .append("password=").append(PASSWORD);

        try (
            Connection connection = DriverManager.getConnection(sb.toString());
            Statement stmt = (Statement) connection.createStatement()
        ){
            // guava check
            Preconditions.checkNotNull(connection);
            // AutoCloseable since java 1.7
            ResultSet result = stmt.executeQuery(SELECT_SQL);

            while (result.next()) {
                Printer.println(result.getString(1));
                Printer.println(result.getString(2));
                Printer.println(result.getString(3));
                Printer.println(result.getString(4));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
