package team.prometheus.app.sample.callback;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import team.prometheus.app.sample.utils.Printer;

/**
 * Created on 16/6/4
 * [Function]
 *
 * @author edwin
 * @version 1.1.0
 * @since 1.1.0
 */
@Getter @Setter
@AllArgsConstructor
public class Task {

    private String name;

    public void execute(String msgA,String msgB, Callback callback) {

        Printer.println("Requesting msg from client...");
        Printer.println("Clean up database...");
        callback.callback(msgA + msgB);
        Printer.println("Take a snap...");
        Printer.println(">> End <<");
    }

    public void print(){
        Printer.println(name);
    }
}
