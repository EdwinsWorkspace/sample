package team.prometheus.app.sample.multithread.runnable;

/**
 * Created on 16/6/6
 * [Function]
 *
 * @author edwin
 * @version 1.1.0
 * @since 1.1.0
 */
public class RunnableDemo {

    public static void main(String[] args){
        new Thread(new RunnableThread(1)).start();
    }
}
