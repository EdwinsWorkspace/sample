package team.prometheus.app.sample.interfaces.java8;

import team.prometheus.app.sample.utils.Printer;

/**
 * Created on 16/6/11
 * [Function]
 *
 * @author edwin
 * @version 1.1.0
 * @since 1.1.0
 */
public class Java8Demo {
    public static void main(String[] args) {

        Printer.println(new Java8Interface() {
            @Override
            public double calculate() {
                return 0; // always zero
            }
        }.sqrt(10));


        // use lambda
        Java8Interface java8Interface = () -> 0;
        Printer.println(java8Interface.sqrt(100));

        Printer.println(
                ( (Java8Interface)(() -> 0) ).sqrt(10000)); // WTF !

    }
}
