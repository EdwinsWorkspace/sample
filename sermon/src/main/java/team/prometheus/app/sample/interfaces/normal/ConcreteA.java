package team.prometheus.app.sample.interfaces.normal;

import team.prometheus.app.sample.utils.Printer;

/**
 * Created on 2016/6/2
 * [Function]
 *
 * @author zhangshuai
 * @version 1.0.0
 * @since 1.0.0
 */
public class ConcreteA extends AbstractEntity {

    @Override public void operations() {
        Printer.println("Eating...");
        abstractMethod();
        Printer.println(super.name);
        this.name = "Concrete A";
        Printer.println("End");

        // you can't set NUM like this
        // NUM = 2;
    }
}
