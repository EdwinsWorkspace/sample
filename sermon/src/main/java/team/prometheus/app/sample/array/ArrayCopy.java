package team.prometheus.app.sample.array;

import team.prometheus.app.sample.utils.Printer;

import java.util.Arrays;

/**
 * Created on 2016/6/10
 * [Function]
 * Describes 3 ways about how to copy a array to another
 *
 * @author zhangshuai
 * @version 1.0.0
 * @since 1.0.0
 */
public class ArrayCopy {
    public static void main (String[] args) {

        // 0.definitions
        int[] nums = {12,3,4,5,6,234,1,6234,67345,17,69};
        int[] copy = new int[nums.length];

        // 1. copy with a loop
        for (int i=0 ; i<nums.length ; i++) {
            copy[i] = nums[i];
        }
        Printer.println("copy1:", Arrays.toString(copy));

        // 2. Or... System.arraycopy(...)
        int[] copy2 = new int[nums.length];
        System.arraycopy(nums, 0, copy2, 0, nums.length);
        Printer.println("copy2:", Arrays.toString(copy2));

        // 3. Or... Arrays.copyOf(...)
        int[] copy3 = Arrays.copyOf(nums, nums.length);
        Printer.println("copy3:", Arrays.toString(copy3));

    }
}
