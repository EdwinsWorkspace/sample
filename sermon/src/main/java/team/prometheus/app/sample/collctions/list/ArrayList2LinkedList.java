package team.prometheus.app.sample.collctions.list;

import team.prometheus.app.sample.utils.Printer;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * Created on 16/6/11
 * [Function]
 *
 * @author edwin
 * @version 1.1.0
 * @since 1.1.0
 */
public class ArrayList2LinkedList {

    public static void main (String[] args){
        List<Integer> list = Arrays.asList(1,2,3,4,5);
        list.forEach((t) -> Printer.println(t)); // java 8

        Printer.println("Hashcode: ", list.hashCode());

        LinkedList l = (LinkedList) list; // So, you can't transfer a array list to linked one.

        // TODO: How to make it possible ?

        Printer.println("Hashcode: ", list.hashCode());

    }
}
