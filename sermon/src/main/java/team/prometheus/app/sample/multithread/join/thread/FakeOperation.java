package team.prometheus.app.sample.multithread.join.thread;

import team.prometheus.app.sample.utils.Printer;

import java.util.List;

/**
 * Created on 16/6/6
 * [Function]
 *
 * @author edwin
 * @version 1.1.0
 * @since 1.1.0
 */
public class FakeOperation extends Thread{

    private String operation;

    private List<Thread> threads;

    public FakeOperation(String operation) {
        this.operation = operation;
    }

    public void setThreads(List threads){
        this.threads = threads;
    }

    @Override
    public synchronized void run() {
        for (int i=0; i<10; i++) {
            // tricky operations
            Printer.repeatedPrint("--");
            // who is working...
            Printer.println(">>", i, "<< doing operation ", operation, "...");
            // list others status...
            for (Thread t : threads) {
                Printer.println(">>", ((FakeOperation) t).operation, " states<<", t.getState());
            }
            // put it wait
            if (i == 5) {
                try {
                    wait(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            Printer.repeatedPrint("--");
        }
    }
}
