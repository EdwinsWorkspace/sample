package team.prometheus.app.sample.array;

import team.prometheus.app.sample.utils.Printer;

import java.util.Arrays;

/**
 * Created on 2016/6/10
 * [Function]
 * Usage of 4 methods of {@link Arrays}
 * @author zhangshuai
 * @version 1.0.0
 * @since 1.0.0
 */
public class ArraysDemo {

    public static void main (String[] args) {
        int[] nums = {12,3,4,5,6,234,1,6234,67345,17,69};
        Printer.println("Original:", Arrays.toString(nums));
        Printer.repeatedPrint("--");

        // 1. sort
        Arrays.sort(nums);
        Printer.println("After sorted:", Arrays.toString(nums));
        Printer.repeatedPrint("--");

        /**
         * 2. binarySearch
         * must be sorted
         */
        Printer.println(Arrays.binarySearch(nums, 12));
        Printer.repeatedPrint("--");

        // 3. fill
        int[] notInitArray = new int[10];
        notInitArray[2] = 23;
        Arrays.fill(notInitArray,1);
        Printer.println("After filled:", Arrays.toString(notInitArray));
        Printer.repeatedPrint("--");

        //4. equals
        Printer.println(Arrays.equals(notInitArray, nums));
        Printer.repeatedPrint("--");
    }
}
