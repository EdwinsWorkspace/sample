package team.prometheus.app.sample.annotation.test;

import lombok.extern.slf4j.Slf4j;
import team.prometheus.app.sample.annotation.TesterInfo;
import team.prometheus.app.sample.utils.Printer;

import java.lang.annotation.Annotation;

/**
 * Created on 2016/5/20
 * [Function]
 *
 * @author zhangshuai
 * @version 1.0.0
 * @since 1.0.0
 */
@Slf4j
public class TestSuite {

    public static void main(String... args) {
        log.info("Testing...");

        int paased = 0, failed = 0, count = 0, ignore = 0;

        Class<TestAnnotations> obj = TestAnnotations.class;

        if (obj.isAnnotationPresent(TesterInfo.class)) {
            Annotation annotation = obj.getAnnotation(TesterInfo.class);
            TesterInfo testerInfo = (TesterInfo) annotation;

            Printer.println(testerInfo.priority());
            Printer.println(testerInfo.createdBy());

            int tagLength = testerInfo.tags().length;
            for (String tag : testerInfo.tags()) {
                if (tagLength > 1) {
                    Printer.println(tag + ", ");
                } else {
                    Printer.println(tag);
                }
                tagLength--;
            }

            Printer.println(testerInfo.lastModified());
        }
    }
}
