package team.prometheus.app.sample.sort.bubble;

import team.prometheus.app.sample.utils.Printer;

import java.util.Arrays;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created on 2016/6/7
 * [Function]
 * Basic bubble sort
 * Time complex; O(n²)
 *
 * @author zhangshuai
 * @version 1.0.0
 * @since 1.0.0
 */
public class BubbleSort {

    static int[] target = new int[]{
        32, 34, 100,
        80, 45, 23,
        1, 35, 20,
        67, 93, 33};

    static final AtomicInteger counter = new AtomicInteger();

    public static void main(String[] args){
        Printer.repeatedPrint("--");
        Printer.println(">>Original<< ", Arrays.toString(target));
        Printer.println(">>[Start Sorting...]");

        // bubble sort
        bubbleSort(target);

        Printer.println(">>[Finished]");
        Printer.println(">>Results<< ", Arrays.toString(target));
        Printer.repeatedPrint("--");
        Printer.println(target.length, " numbers");
        Printer.println(counter, " times");
        Printer.println("Time complex; O(n²)");
    }


    public static void bubbleSort(int[] target) {
        if (target == null || target.length == 0) return;

        for (int index=0; index< target.length; index++) {
            for (int anchor = 1; anchor< target.length - index; anchor++) {
                if (target[anchor] < target[anchor-1]) {
                    switchElements(target, anchor, anchor-1);
                }
            }
            Printer.println(">>Round",index,"<<", Arrays.toString(target));
        }
    }

    private static void switchElements(int[] target, int anchorA, int anchorB) {
        int temp = target[anchorA];
        target[anchorA] = target[anchorB];
        target[anchorB] = temp;
        counter.incrementAndGet();
    }
}
