package team.prometheus.app.sample.multithread.producer;

import team.prometheus.app.sample.utils.Printer;

/**
 * Created on 16/6/7
 * [Function]
 *
 * @author edwin
 * @version 1.1.0
 * @since 1.1.0
 */
public class Producer implements Runnable{

    Queue queue;

    public Producer(Queue queue) {
        this.queue = queue;
    }

    @Override
    public void run() {
        for (int i=0; i<100; i++) {
            try {
                produce(i);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }

    public void produce(int i) throws InterruptedException {
        if (queue.queue != null) {
            queue.queue.put(i,i);
            Printer.println("Produced: ", i);
        }

        if (queue.queue.size() >= 10) {
            Printer.println("Waiting for sufficient space");
            Thread.sleep(100);
        }
    }
}
