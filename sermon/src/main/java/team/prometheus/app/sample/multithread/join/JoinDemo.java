package team.prometheus.app.sample.multithread.join;

import team.prometheus.app.sample.multithread.join.thread.FakeOperation;
import team.prometheus.app.sample.utils.Printer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 16/6/6
 * [Function]
 *
 * @author edwin
 * @version 1.1.0
 * @since 1.1.0
 */
public class JoinDemo {

    public static void main(String[] args) throws InterruptedException {

        long startTime = System.currentTimeMillis();

        List<Thread> list = new ArrayList<>(3);

        // create operation A,B,C
        FakeOperation a = new FakeOperation("A");
        FakeOperation b = new FakeOperation("B");
        FakeOperation c = new FakeOperation("C");

        // notify all threads alive
        list.add(a); list.add(b); list.add(c);
        a.setThreads(list);
        b.setThreads(list);
        c.setThreads(list);

        // execute & join
        a.start();
        a.join();
        b.start();
        c.start();

        // you want calculate time at the end.
        b.join();
        c.join();

        long stopTime = System.currentTimeMillis();

        float execTime = stopTime - startTime;

        Printer.println(execTime, "ms");
    }
}
