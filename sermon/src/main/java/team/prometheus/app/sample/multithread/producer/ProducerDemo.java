package team.prometheus.app.sample.multithread.producer;

/**
 * Created on 16/6/7
 * [Function]
 *
 * @author edwin
 * @version 1.1.0
 * @since 1.1.0
 */
public class ProducerDemo {

    public static void main(String[] args) {
        Queue queue = new Queue();

        new Thread(new Consumer(queue)).start();

        new Thread(new Producer(queue)).start();

    }
}
