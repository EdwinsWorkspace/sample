package team.prometheus.app.sample.basic;

import team.prometheus.app.sample.utils.Printer;

/**
 * Created on 16/6/6
 * [Function]
 * Verified default value of each variable
 *
 * @author edwin
 * @version 1.1.0
 * @since 1.1.0
 */
public class DefaultVariableValue {

    int i;
    String s;
    boolean b;

    public void doingSomthing(){
        Printer.println(i);
        Printer.println(s);
        Printer.println(b);
    }


    public static void main(String[] args){
        new DefaultVariableValue().doingSomthing();
    }
}
