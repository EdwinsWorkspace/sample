package team.prometheus.app.sample.basic;

/**
 * Created on 16/6/27
 * [Function]
 *
 * @author edwin
 * @version 1.1.0
 * @since 1.1.0
 */
public class SwithType {

    public static void main(String args[]) {

        String msg = "Success";

        switch (msg) {
            case "Success": System.out.print("success");
                break;
            default: System.out.print("fail");
        }

        System.out.println();

        long m = 100L;
        switch ((int)m) {
            case 10: System.out.print("10L");
                break;
            default: System.out.print("more than 10L");
        }
    }
}
