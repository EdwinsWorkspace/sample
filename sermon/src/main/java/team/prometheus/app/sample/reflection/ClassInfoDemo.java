package team.prometheus.app.sample.reflection;

import team.prometheus.app.sample.utils.Printer;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Parameter;
import java.util.Arrays;

/**
 * Created on 2016/6/12
 * [Function]
 * Reflection means you can dynamically get all class info.
 *
 * [Note]
 * You can't create a Class object, it's created by JVM
 *
 * @author zhangshuai
 * @version 1.0.0
 * @since 1.0.0
 */
public class ClassInfoDemo {

    public static void main(String[] args){
        // Let's explore this class
        CustomFoo forNameFoo = new CustomFoo();

        // get its class definition
        Class clazz = forNameFoo.getClass();

        printClassInfo(clazz);

        printMethodsInfo(clazz);

        printFieldsInfo(clazz);

    }

    private static void printClassInfo(Class clazz) {
        Printer.repeatedPrint("--");
        Printer.println("Class:", clazz.getName());
        Printer.repeatedPrint("--");

        Printer.println("isInterface:", clazz.isInterface());
        Printer.println("isPrimitive:", clazz.isPrimitive());

        Printer.println("getSuperclass:", clazz.getSuperclass());
        Printer.println("getGenericSuperclass:", clazz.getGenericSuperclass());

        Printer.println("getModifiers:", Modifier.toString(clazz.getModifiers()));
        Printer.println("getInterfaces:", Arrays.toString(clazz.getInterfaces()));
        Printer.println("getModifiers:", Arrays.toString(clazz.getInterfaces()));
        Printer.println("getDeclaredFields:", Arrays.toString(clazz.getDeclaredFields()));

        Printer.repeatedPrint("--");
    }

    private static void printMethodsInfo(Class clazz) {
        Printer.repeatedPrint("--");
        Printer.println("Methods:");
        Printer.repeatedPrint("--");
        for (Method method : clazz.getMethods()) {
            Printer.repeatedPrint("-");
            Printer.println("Method:", method.getName());
            Printer.repeatedPrint("-");
            // since java 8
            for(Parameter parameter : method.getParameters()) {
                Printer.println("{");
                Printer.println(" - getModifiers:", Modifier.toString(parameter.getModifiers()));
                Printer.println(" - getType:", parameter.getType().getName());
                Printer.println(" - getName:", parameter.getName());
                Printer.println("}");
            }
        }

        Printer.repeatedPrint("--");
    }

    private static void printFieldsInfo(Class clazz) {
        Printer.repeatedPrint("--");
        Printer.println("Fields:");
        Printer.repeatedPrint("--");

        Field[] fields = clazz.getFields();
        Printer.println("Only public methods");
        Printer.println(Arrays.toString(fields));

        Field[] allFields = clazz.getDeclaredFields();
        Printer.println(Arrays.toString(allFields));

        for (Field field : allFields) {
            field.setAccessible(true);
            Printer.println("[", field.getType().getSimpleName(), " | ", field.getName(), " | ", Modifier.toString(field.getModifiers()), "]");
        }

        Printer.repeatedPrint("--");
    }
}
