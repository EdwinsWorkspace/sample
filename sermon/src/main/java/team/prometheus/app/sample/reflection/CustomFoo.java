package team.prometheus.app.sample.reflection;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * Created on 2016/6/12
 * [Function]
 *
 * @author zhangshuai
 * @version 1.0.0
 * @since 1.0.0
 */
@Slf4j
@Getter @Setter
public class CustomFoo implements Cloneable{

    private String name;

    static {
        log.info("Static block of CustomFoo.");
    }

    public CustomFoo() {
        log.info("Constructor of CustomFoo.");
    }

    public void drive(String driver, int version){
        log.info("drive");
    }

    public void drive(String driver, String dest, int version){
        log.info("drive to ", dest);
    }


}
