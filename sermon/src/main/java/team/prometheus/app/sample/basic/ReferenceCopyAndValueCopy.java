package team.prometheus.app.sample.basic;

import team.prometheus.app.sample.utils.Printer;

/**
 * Created on 2016/5/19
 * This is a simple sample to tell differences between reference copy & value copy
 *
 * [Conclusion]
 * If you new a STRING, it will create a new object instead of return existed one,
 * that's why "==" failed;
 * But "equals(..)" compares the hashCode which related to the real content,
 * so it passed.
 *
 * @author zhangshuai
 * @version 1.0.0
 * @since 1.0.0
 */
public class ReferenceCopyAndValueCopy {

    public static void main(String... args) {

        // init variables
        String a = "123";
        String b = "1" + "23";
        String c = new String("123");
        String temp = "1";
        String d = temp + "23";

        Printer.repeatedPrint("#");

        Printer.println("String a = \"123\"");
        Printer.println("String b = \"1\" + \"23\"");
        Printer.println("String c = new String(\"123\")");
        Printer.println("String d = temp + \"23\"");

        // show results
        Printer.println("a.equals(b):", a.equals(b)); // true
        Printer.println("b.equals(c):", b.equals(c)); // true
        Printer.println("a.equals(c):", a.equals(c)); // true
        Printer.println("a.equals(c):", a.equals(d)); // true

        Printer.println(a == b); // true
        Printer.println(b == c); // false
        Printer.println(a == c); // false
        Printer.println(a == d); // false
    }
}
