package team.prometheus.app.sample.jdbc;

import com.mysql.jdbc.PreparedStatement;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created on 16/6/11
 * [Function]
 *
 * @author edwin
 * @version 1.1.0
 * @since 1.1.0
 */
public class PreparedStatementDemo {

    /**
     * -----------------------------------------------------------------
     * TODO: Please move this to a property file, and try to use Property util
     * -----------------------------------------------------------------
     */
    private final static String URL = "jdbc:mysql://localhost:3306/post";
    private final static String USER_NAME = "test";
    private final static String PASSWORD = "123456";
    private final static String SELECT_SQL = "SELECT * FROM post;";

    public static void main(String[] args) throws ClassNotFoundException {

        // load driver class
        Class.forName("com.mysql.jdbc.Driver");

        // build url
        StringBuilder sb = new StringBuilder();
        sb = sb.append(URL).append("?")
                .append("user=").append(USER_NAME).append("&")
                .append("password=").append(PASSWORD);

        try ( // since java 1.7
            Connection connection = DriverManager.getConnection(sb.toString());
            PreparedStatement stmt = (PreparedStatement) connection.prepareStatement(SELECT_SQL)
        ) {
            // ResultSet is AutoCloseable
            ResultSet result = stmt.executeQuery(SELECT_SQL);

        } catch (SQLException e) {
            e.printStackTrace();
        };
    }

}
