package team.prometheus.app.sample.callback;

import team.prometheus.app.sample.utils.Printer;

import java.util.Arrays;

/**
 * Created on 16/6/4
 * [Function]
 * A callback demo,
 * You can delay implement operations.
 * Of course you can return a result.
 *
 * @author edwin
 * @version 1.1.0
 * @since 1.1.0
 */
public class CallbackDemo {
    public static void main(String[] args) {
        Task taskA = new Task("A");
        Task taskB = new Task("B");

        // lambda expression since java 8
        taskA.execute("A", "B",
                (msg) ->
                {
                    // normal action
                    Printer.println(">>Callback Invoked<<", Arrays.toString(msg));
                    // Here! Weird, huh~
                    // you can invoke other object in your callback
                    // if its a SET operation, then you can send message to others.
                    taskB.print();
                }
        );

    }
}
