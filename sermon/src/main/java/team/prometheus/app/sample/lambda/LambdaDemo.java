package team.prometheus.app.sample.lambda;

import team.prometheus.app.sample.utils.Printer;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created on 16/6/11
 * [Function]
 *
 * @author edwin
 * @version 1.1.0
 * @since 1.1.0
 */
public class LambdaDemo {

    public static void main(String[] args) {
        List<String> list = Arrays.asList("4","2","6","7");
        Collections.sort(list, (a,b)-> b.compareTo(a));

        Converter<String, Integer> converter = (from -> Integer.valueOf(from));
        Integer res = converter.convert("123");
        Printer.println(res);
    }
}
