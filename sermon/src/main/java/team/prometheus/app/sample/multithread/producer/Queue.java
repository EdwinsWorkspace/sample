package team.prometheus.app.sample.multithread.producer;

import java.util.concurrent.ConcurrentHashMap;

/**
 * Created on 16/6/7
 * [Function]
 *
 * @author edwin
 * @version 1.1.0
 * @since 1.1.0
 */
public class Queue {

    public final ConcurrentHashMap<Integer,Integer> queue = new ConcurrentHashMap();
    public static final int MAX_SIZE = 10;

}
