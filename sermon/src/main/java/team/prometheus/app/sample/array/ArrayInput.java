package team.prometheus.app.sample.array;

import team.prometheus.app.sample.utils.Printer;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created on 2016/6/10
 * [Function]
 *
 * @author zhangshuai
 * @version 1.0.0
 * @since 1.0.0
 */
public class ArrayInput {

    public static void main (String[] args) {

        // STEP 1: verify basic reference modification
        int[] intArray = new int[10]; // define a array with init size
        Printer.println(Arrays.toString(intArray)); // verify default value
        int i = intArray[0];
        i = 10; // modified the "reference"
        Printer.println(Arrays.toString(intArray)); // verify its a value copy

        Printer.repeatedPrint("--"); // ------------------------------------------------------------

        // STEP 2: verify object reference modification
        Map[] mapArray = {new HashMap<>(), new ConcurrentHashMap<>()};
        Printer.println(Arrays.toString(mapArray));

        Map<Object, Object> customMap = mapArray[0];
        customMap.put("1+1", "2");
        Printer.println(Arrays.toString(mapArray)); // verify its a address copy

        customMap = mapArray[1];
        customMap.put(2, true);
        Printer.println(Arrays.toString(mapArray)); // verify different type

        Printer.repeatedPrint("--"); // ------------------------------------------------------------

        // STEP 3: nothing...
        Collection[] collectionArray = {new ArrayList<String>(), new ArrayList<Integer>(), new HashSet<>()};
    }
}
