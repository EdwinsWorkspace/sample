package team.prometheus.app.sample.multithread.thread;

/**
 * Created on 16/6/3
 * [Function]
 *
 * @author edwin
 * @version 1.1.0
 * @since 1.1.0
 */
public class ThreadDemo {

    public static void main(String[] args) {
        int num = 0;
//        while (++num < 30) {
            new MyThread(num).start();
//        }
    }
}
