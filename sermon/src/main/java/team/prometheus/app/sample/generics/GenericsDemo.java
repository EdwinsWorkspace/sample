package team.prometheus.app.sample.generics;

import team.prometheus.app.sample.utils.Printer;

/**
 * Created on 16/6/11
 * [Function]
 *
 * @author edwin
 * @version 1.1.0
 * @since 1.1.0
 */
public class GenericsDemo {

    public static void main (String[] args) {
        GenericFoo<Integer> foo = new GenericFoo<>();
        foo.setEntity(1);

        Printer.println(foo.getEntity());

        GenericFoo<String> stringGenericFoo = new GenericFoo<>();
        stringGenericFoo.setEntity("Kitty");

        Printer.println(stringGenericFoo.getEntity());

        // This generic must extends your custom abstract class,
        // Or it will fail the compile.
        // CustomGenerics<Integer> custom = new CustomGenerics();
    }
}
