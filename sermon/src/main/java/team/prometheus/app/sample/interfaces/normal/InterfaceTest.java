package team.prometheus.app.sample.interfaces.normal;

import team.prometheus.app.sample.utils.Printer;

/**
 * Created on 2016/6/2
 * [Function]
 *
 * @author zhangshuai
 * @version 1.0.0
 * @since 1.0.0
 */
public class InterfaceTest {

    public static void main(String[] args) {
        DemoInterface demo = new ConcreteA();
        demo.operations();
        // This is gonna show "Concrete A"
        // Because it's overwrote.
        Printer.println(((AbstractEntity)demo).name);

        demo = new ConcreteB();
        demo.operations();

        // This is gonna show abstract one
        Printer.println(((AbstractEntity)demo).name);
    }
}
