package team.prometheus.app.sample.collctions.list.arraylist;

import team.prometheus.app.sample.utils.Printer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

/**
 * Created on 2016/5/25
 * [Function]
 * Prove that you can regard a dyadic array a list of array
 *
 * @author zhangshuai
 * @version 1.0.0
 * @since 1.0.0
 */
public class DyadicArrayTest {

    public static void main(String[] args) {
        Object[][] test = {{"case1.json"}, {"case2.json"}, {"case3.json"}, {"case4.json"}, {"case5.json"}, {"case6.json"}, {"case7.json"}, {"case8.json"}, {"case9.json"}};

        // method 1
        Collection<Object[]> parameters = new ArrayList<>(test.length);
        Collections.addAll(parameters, test);
        Printer.println(parameters);

        Printer.println(Printer.repeat("#", 30));

        // method 2
        Collection<Object[]> parameters2 = new ArrayList<>(test.length);
        for (Object[] pattern:test) {
            parameters2.add(pattern);
        }
        Printer.println(parameters2);
    }


}
