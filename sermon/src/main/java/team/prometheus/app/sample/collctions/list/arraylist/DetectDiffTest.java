package team.prometheus.app.sample.collctions.list.arraylist;

import team.prometheus.app.sample.utils.Printer;

import java.util.*;

/**
 * Created on 2016/5/30
 * [Function]
 * Provides an algorithm to compare 2 list
 * Time complexity O(m+n)
 *
 * @author zhangshuai
 * @version 1.0.0
 * @since 1.0.0
 */
public class DetectDiffTest {

    public static void main(String[] args) {

        // init lists
        final int lengthA = 50;
        final int lengthB = 100;

        List<Integer> listA = new ArrayList<>(lengthA);
        List<Integer> listB = new ArrayList<>(lengthB);

        for (int i=0; i <= lengthA; i++) {
            listA.add(2 * i);
        }

        for (int i=0; i <= lengthB; i++) {
            listB.add(i);
        }

        // get aggregation set
        Map<Integer, Integer> aggregationMap = getAggregationMap(listA, listB);

        Printer.repeatedPrint("##");
        Printer.println("[COUNT MAP]");
        Printer.println(aggregationMap);
        Printer.repeatedPrint("##");

        Printer.repeatedPrint("##");
        Printer.println("[DIFF ITEMS]");
        Printer.println(getDiff(aggregationMap));
        Printer.repeatedPrint("##");

        Printer.repeatedPrint("##");
        Printer.println("[SAME ITEMS]");
        Printer.println(getSame(aggregationMap));
        Printer.repeatedPrint("##");

    }

    /**
     * Get aggregated collections of 2 lists
     * Put numerical item as key
     * Put num of each item as value
     * @param listA
     * @param listB
     * @return
     */
    private static Map getAggregationMap(List listA, List listB) {
        Map<Integer, Integer> countMap = new HashMap<>();
        List<Integer> max;
        List<Integer> min;

        if (listA.size() < listB.size()) {
            max = listA;
            min = listB;
        } else{
            max = listB;
            min = listA;
        }

        // loop list A
        for (int num : max) {
            countMap.put(num, 1);
        }
        // loop list B
        for (int num : min) {
            Integer previousCount = countMap.put(num, 1);
            if (previousCount != null && previousCount > 0) {
                countMap.put(num, ++previousCount);
            }
        }

        return countMap;
    }

    /**
     *
     * @param countMap
     * @return
     */
    private static List getDiff(Map<Integer, Integer> countMap) {
        List<Integer> diff = new ArrayList<>();

        for (Map.Entry<Integer, Integer> entry : countMap.entrySet()) {
            if (entry.getValue() == 1) {
                diff.add(entry.getKey());
            }
        }

        return diff;
    }

    private static List getSame(Map<Integer, Integer> countMap) {
        List<Integer> same = new ArrayList<>();

        for (Map.Entry<Integer, Integer> entry : countMap.entrySet()) {
            if (entry.getValue() > 1) {
                same.add(entry.getKey());
            }
        }

        return same;
    }
}
