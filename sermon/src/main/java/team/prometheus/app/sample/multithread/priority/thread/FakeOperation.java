package team.prometheus.app.sample.multithread.priority.thread;

import java.util.List;

import team.prometheus.app.sample.utils.Printer;

/**
 * Created on 16/6/6
 * [Function]
 *
 * @author edwin
 * @version 1.1.0
 * @since 1.1.0
 */
public class FakeOperation extends Thread{

    private String operation;

    private List<Thread> threads;

    public FakeOperation(String operation) {
        this.operation = operation;
    }

    public void setThreads(List threads){
        this.threads = threads;
    }

    @Override
    public synchronized void run() {
        for (int i=0; i<10; i++) {
            // tricky operations
            Printer.repeatedPrint("--");
            // who is working...
            Printer.println(">>", i, "<< doing operation ", operation, "...");
            // list others status...
            for (Thread t : threads) {
                Printer.println(">>", ((FakeOperation) t).operation, " states<<", t.getState());
            }
            Printer.repeatedPrint("--");
        }
    }
}
