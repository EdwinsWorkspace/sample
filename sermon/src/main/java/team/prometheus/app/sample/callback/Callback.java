package team.prometheus.app.sample.callback;

/**
 * Created on 16/6/4
 * [Function]
 *
 * @author edwin
 * @version 1.1.0
 * @since 1.1.0
 */
public interface Callback {

    void callback(String ... msg);
}
