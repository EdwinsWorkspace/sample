package team.prometheus.app.sample.lambda;

/**
 * Created on 16/6/11
 * [Function]
 *
 * @author edwin
 * @version 1.1.0
 * @since 1.1.0
 */
@FunctionalInterface
public interface Converter<F, T> {
    T convert(F from);
}
