package team.prometheus.app.sample.basic;

/**
 * Created on 16/6/27
 * [Function]
 *
 * @author edwin
 * @version 1.1.0
 * @since 1.1.0
 */
public class LoopAndBreak {

    public static void main (String[] args) {

        // Case 1
//        for (int i=0 ; i <= 10 ; i++) {
//            for (int j=0 ; j <= 10 ; j++) {
//                System.out.println(String.valueOf(i) + "," + String.valueOf(j));
//                if (i== 7 && j==6) break;
//            }
//        }

        // Case 2
//        ok:
//        for (int i=0 ; i <= 10 ; i++) {
//            for (int j=0 ; j <= 10 ; j++) {
//                System.out.println(String.valueOf(i) + "," + String.valueOf(j));
//                if (i== 7 && j==6) break ok;
//            }
//        }

        // Case 3
        boolean flag = false;
        for (int i=0 ; i <= 10 && !flag ; i++) {
            for (int j=0 ; j <= 10 ; j++) {
                System.out.println(String.valueOf(i) + "," + String.valueOf(j));
                if (i== 7 && j==6) {
                    flag = true;
                    break ;
                }
            }
        }
    }
}
