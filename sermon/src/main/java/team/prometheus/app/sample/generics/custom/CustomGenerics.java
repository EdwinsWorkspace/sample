package team.prometheus.app.sample.generics.custom;

import lombok.Getter;
import lombok.Setter;

/**
 * Created on 16/6/11
 * [Function]
 *
 * @author edwin
 * @version 1.1.0
 * @since 1.1.0
 */
@Getter @Setter
public class CustomGenerics<T extends AbstractGenericsFoo> {

    private T entity;
}
