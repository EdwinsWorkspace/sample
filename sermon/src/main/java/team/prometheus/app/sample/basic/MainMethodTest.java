package team.prometheus.app.sample.basic;

import team.prometheus.app.sample.utils.Printer;

/**
 * Created on 2016/5/19
 * The MAIN method MUST be
 * - public, indicates that it can be called by any object
 * - static, indicates that it can be called without init this class
 * - void, no return value
 * @author zhangshuai
 * @version 1.0.0
 * @since 1.0.0
 */
public class MainMethodTest {

//    private static void main(String[] args) {
//        // some codes
//    }

    public static void main(String[] args) {
        // just testing...
        Printer.println(Boolean.TRUE.equals("true"));
        Printer.println(Boolean.toString(true).equals("true"));
    }
}
