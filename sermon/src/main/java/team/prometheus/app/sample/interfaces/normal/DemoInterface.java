package team.prometheus.app.sample.interfaces.normal;

/**
 * Created on 2016/6/2
 * [Function]
 *
 * @author zhangshuai
 * @version 1.0.0
 * @since 1.0.0
 */
public interface DemoInterface {

    /**
     * This is gonna be shared with all concrete class
     */
    int NUM = 10;

    void abstractMethod();

    void operations();
}
