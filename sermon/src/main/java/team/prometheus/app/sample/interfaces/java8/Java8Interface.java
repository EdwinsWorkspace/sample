package team.prometheus.app.sample.interfaces.java8;

/**
 * Created on 16/6/11
 * [Function]
 *
 * @author edwin
 * @version 1.1.0
 * @since 1.1.0
 */
public interface Java8Interface {
    /**
     * normal one
     * @return
     */
    double calculate();

    /**
     * Supported since java 8
     * @param a
     * @return
     */
    default double sqrt(int a) {
     return Math.sqrt(a);
    }
}
