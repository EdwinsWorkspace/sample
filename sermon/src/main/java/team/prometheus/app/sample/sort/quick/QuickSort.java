package team.prometheus.app.sample.sort.quick;

import team.prometheus.app.sample.utils.Printer;

import java.util.Arrays;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created on 2016/6/7
 * [Function]
 *
 * @author zhangshuai
 * @version 1.0.0
 * @since 1.0.0
 */
public class QuickSort {

    static int[] target = new int[]{
        32, 34, 100,
        80, 45, 23,
        1, 35, 20,
        67, 93, 33};
    static final AtomicInteger counter = new AtomicInteger();

    public static void main(String[] args){
        Printer.repeatedPrint("--");
        Printer.println(">>Original<< ", Arrays.toString(target));
        Printer.println(">>[Start Sorting...]");

        // bubble sort
        quickSort(target);

        Printer.println(">>[Finished]");
        Printer.println(">>Results<< ", Arrays.toString(target));
        Printer.repeatedPrint("--");
        Printer.println(target.length, " numbers");
        Printer.println(counter, " times");
        Printer.println("Time complex; O(n²)");
    }


    public static void quickSort(int[] target) {
        if (target == null || target.length == 0) return;


    }

    private static void switchElements(int[] target, int anchorA, int anchorB) {
        int temp = target[anchorA];
        target[anchorA] = target[anchorB];
        target[anchorB] = temp;
        counter.incrementAndGet();
    }
}
