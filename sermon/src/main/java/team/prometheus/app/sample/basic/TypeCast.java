package team.prometheus.app.sample.basic;

/**
 * Created on 16/6/27
 * [Function]
 *
 * @author edwin
 * @version 1.1.0
 * @since 1.1.0
 */
public class TypeCast {

    public static void main(String[] args) {
        short s = 1;
        s = (short) (s + 2);
        System.out.println(s);

        s += 1;
        System.out.println(s);
    }
}
