package team.prometheus.app.sample.multithread.lock.lock;

import team.prometheus.app.sample.utils.Printer;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created on 2016/6/7
 * [Function]
 *
 * @author zhangshuai
 * @version 1.0.0
 * @since 1.0.0
 */
public class SimpleLockDemo {

    static int i = 1;
    static Lock lock = new ReentrantLock();

    public static void main(String[] args){

        new Thread(() -> {
                lock.lock();
                try {
                    for (int k=0; k<10; k++) {
                        Printer.println(">>1<<", i++);
                    }
                } finally {
                    try {
                        Printer.println("sleeping...");
                        Thread.sleep(1000);
                        Printer.println("Awake...");
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    lock.unlock();
                }
        }).start();

        new Thread(() -> {
                lock.lock();
                Printer.println("Got Lock...");
                try {
                    for (int k=0; k<10; k++) {
                        Printer.println(">>2<<", i++);
                    }
                } finally {
                    lock.unlock();
                }
        }).start();
    }
}
