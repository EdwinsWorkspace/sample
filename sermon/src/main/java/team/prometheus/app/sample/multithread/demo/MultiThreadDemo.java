package team.prometheus.app.sample.multithread.demo;

import team.prometheus.app.sample.utils.Printer;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created on 2016/6/7
 * [Function]
 *
 * @author zhangshuai
 * @version 1.0.0
 * @since 1.0.0
 */
public class MultiThreadDemo implements Runnable{

    private static int i;
    private static volatile Integer vi = 0;
    private static AtomicInteger ai = new AtomicInteger();
    private static Integer si = 0;
    private static int ri;
    private static AtomicInteger flag = new AtomicInteger();
    private Lock lock = new ReentrantLock();

    @Override public void run() {
        for (int k=0; k<333333; k++) {
            i++;
            vi++;
            ai.incrementAndGet();
            synchronized (si) {
                si++;
            }
            lock.lock();
            try {
                ri++;
            } finally {
                lock.unlock();
            }
        }
        flag.incrementAndGet();
    }

    public static void main(String[] args) throws InterruptedException {
        MultiThreadDemo demo = new MultiThreadDemo();
        MultiThreadDemo demo2 = new MultiThreadDemo();

        ExecutorService exec1 = Executors.newCachedThreadPool();
        ExecutorService exec2 = Executors.newCachedThreadPool();

        exec1.execute(demo);
        exec2.execute(demo2);

        while (true) {
            if (flag.intValue() == 2) {
                Printer.println("i: ",i);
                Printer.println("vi: ",vi);
                Printer.println("ri: ",ri);
                Printer.println("ai: ",ai);
                Printer.println("si: ",si);
                break;
            }
        }
        exec1.shutdown();
        exec2.shutdown();
    }
}
