package team.prometheus.app.sample.keyword;

import lombok.AllArgsConstructor;
import team.prometheus.app.sample.utils.Printer;

/**
 * Created on 16/6/3
 * [Function]
 *
 * @author edwin
 * @version 1.1.0
 * @since 1.1.0
 */
public class TransientAndVolatile {

    public static void main(String[] args) {
        Template template = new Template(1,2);
        Printer.println(template.a, template.b);
    }

}

@AllArgsConstructor
strictfp class Template{
    transient int a;
    volatile int b;
}
