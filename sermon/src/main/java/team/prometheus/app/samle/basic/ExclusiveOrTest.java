package team.prometheus.app.samle.basic;

/**
 * Created on 2016/8/15
 * [Function]
 *
 * @author zhangshuai
 * @version 1.0.0
 * @since 1.0.0
 */
public class ExclusiveOrTest {

    public static void main(String[] args) {
        boolean a = true;
//        boolean b = false;
        boolean b = true;

        if (a^b) {
            System.out.println("True");
        } else {
            System.out.println("False");
        }
    }
}
