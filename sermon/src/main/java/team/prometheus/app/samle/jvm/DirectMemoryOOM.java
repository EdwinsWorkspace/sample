package team.prometheus.app.samle.jvm;

import sun.misc.Unsafe;

import java.lang.reflect.Field;

/**
 * Created on 2016/8/4
 * [Function]
 * -Xmx20M -XX:MaxDirectMemorySize=10M
 *
 * @author zhangshuai
 * @version 1.0.0
 * @since 1.0.0
 */
public class DirectMemoryOOM {
    private static final int _1MB = 1024 * 1024;
    static int counter = 1;

    public static void main(String[] args) throws IllegalAccessException {
        Field unsafeField = Unsafe.class.getDeclaredFields()[0];
        unsafeField.setAccessible(true);
        Unsafe unsafe = (Unsafe) unsafeField.get(null);
        while (true) {
            unsafe.allocateMemory(_1MB);
            System.out.println(counter++);
        }
    }
}
