package team.prometheus.app.samle.collctions.list.arraylist;

import team.prometheus.app.samle.utils.Printer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 2016/7/22
 * [Function]
 *
 * @author zhangshuai
 * @version 1.0.0
 * @since 1.0.0
 */
public class ArrayListMemDebugTest {

    public static void main (String[] args) {
        List<String> fixedList = new ArrayList(4);
        fixedList.add("zhang");
        fixedList.add("san");
        fixedList.add("feng");
        fixedList.add("haha");
        Printer.println(fixedList);

        List<String> volatileList = fixedList;
        volatileList.add("nihao");
        Printer.println(volatileList);
    }
}
