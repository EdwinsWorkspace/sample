package team.prometheus.app.samle.jvm;

import lombok.extern.slf4j.Slf4j;

/**
 * Created on 2016/8/4
 * [Function]
 * -Xss2m
 *
 * @author zhangshuai
 * @version 1.0.0
 * @since 1.0.0
 */
@Slf4j
public class VMStackOOM2 {

    private int threadId = 0;

    private void loop() {
        while (true) {

        }
    }

    public void stackLeakByThread() {
        while (true) {
            new Thread(() -> {
                loop();
            }).start();
            threadId ++;
            System.out.println(threadId);
        }
    }

    public static void main(String[] args) {
        VMStackOOM2 oom2 = new VMStackOOM2();
        oom2.stackLeakByThread();
    }
}
