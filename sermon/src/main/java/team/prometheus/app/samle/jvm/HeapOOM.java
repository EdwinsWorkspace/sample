package team.prometheus.app.samle.jvm;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 2016/8/4
 * [Function]
 * VM args: c
 *
 * @author zhangshuai
 * @version 1.0.0
 * @since 1.0.0
 */
public class HeapOOM {
    static class OOMObject {
        static int i = 0;
        OOMObject(){
            System.out.println(i++);
        }
    }

    public static void main(String[] args) {
        List<OOMObject> list = new ArrayList<>();
        while (true) {
            list.add(new OOMObject());
        }
    }
}
