package team.prometheus.app.samle.jvm;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * Created on 2016/8/4
 * [Function]
 * -XX:PermSize=10M -XX:MaxPermSize=10M
 *
 * @author zhangshuai
 * @version 1.0.0
 * @since 1.0.0
 */
public class MethodAreaOOM {

    static int counter = 1;
    public static void main(String[] args) {
        while(true) {
            Enhancer enhancer = new Enhancer();
            enhancer.setSuperclass(OOMObject.class);
            enhancer.setUseCache(false);
            enhancer.setCallback(new MethodInterceptor() {
                @Override public Object intercept(Object o, Method method, Object[] objects,
                    MethodProxy methodProxy) throws Throwable {
                    return methodProxy.invokeSuper(o, objects);
                }
            });
            enhancer.create();
            System.out.println(counter++);
        }
    }

    static class OOMObject{

    }
}
