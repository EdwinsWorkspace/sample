package team.prometheus.app.samle.collctions.map;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created on 2016/8/11
 * [Function]
 *
 * @author zhangshuai
 * @version 1.0.0
 * @since 1.0.0
 */
public class HashOrderDifferenceTest {
    public static void main( String[] args )
    {
        /**
         * Using HashMap
         */
        System.out.println( "Using plain hash map with balanced trees:" );

        HashMap<String,String> stringMap = new HashMap();

        for( int i = 0; i < 100; ++i )
        {
            stringMap.put( "index_" + i, String.valueOf( i ) );
        }

//        stringMap.values().forEach( System.out::println );
        for (Map.Entry<String,String> entry : stringMap.entrySet()) {
            System.out.println(entry.getValue());
        }

        /**
         * Using LinkedHashMap
         */
        System.out.println( "Using LinkedHashMap:" );

        LinkedHashMap<String,String> linkedHashMap = new LinkedHashMap();

        for( int i = 0; i < 100; ++i )
        {
            linkedHashMap.put( "index_" + i, String.valueOf( i ) );
        }

//        linkedHashMap.values().forEach( System.out::println );
        for (Map.Entry<String,String> entry : linkedHashMap.entrySet()) {
            System.out.println(entry.getValue());
        }
    }
}
