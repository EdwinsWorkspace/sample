package team.prometheus.app.samle.basic;

/**
 * Created on 2016/8/3
 * [Function]
 *
 * @author zhangshuai
 * @version 1.0.0
 * @since 1.0.0
 */
public class StringTest {

    public static void main(String[] args) {
//        test1();
        test2();
    }


    /**
     *
     */
    private static void test1(){
        String str1 = "a";
        String str2 = "bc";
        String str3 = "a"+"bc"; // 均为常量才入池/
        String str4 = str1+str2; // 变量不入池
        String str5 = "abc";

        System.out.println(str3==str4);
        System.out.println(str3==str5);
        System.out.println(str4==str5);
        str4 = (str1+str2).intern();
        System.out.println(str3==str4);
        System.out.println(str3==str5);
        System.out.println(str4==str5);
    }

    /**
     * intern 方法会记录首次出现的实例引用
     */
    private static void test2() {
        String str1 = new StringBuilder("你好").append("世界").toString();
        System.out.println(str1.intern() == str1);

        String str2 = new StringBuilder("ja").append("va").toString();
        System.out.println(str2.intern() == str2);

    }
}
