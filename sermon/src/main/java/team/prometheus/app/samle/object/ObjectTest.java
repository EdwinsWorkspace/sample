package team.prometheus.app.samle.object;

import team.prometheus.app.samle.utils.Printer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 2016/8/2
 * [Function]
 *
 * @author zhangshuai
 * @version 1.0.0
 * @since 1.0.0
 */
public class ObjectTest {

    public static void main (String[] args) {
        Short s = 34;

        Printer.println(s);
        change(s);
        Printer.println(s);

        List<String> l = new ArrayList<>(2);
        l.add("zhang");
        Printer.println(l);
        change(l);
        Printer.println(l);

    }

    private static void change(List<String> l) {
        l.add("changed");
    }

    private static void change(Short s) {
        s = 78;
    }
}
