package team.prometheus.app.samle.jvm;

import lombok.extern.slf4j.Slf4j;

/**
 * Created on 2016/8/4
 * [Function]
 * -Xss128k
 *
 * @author zhangshuai
 * @version 1.0.0
 * @since 1.0.0
 */
@Slf4j
public class VMStackOOM {
    private int stackLen = 1;

    public void stackLeak() {
        stackLen++;
        stackLeak();
    }

    public static void main(String[] args) {
        VMStackOOM oom = new VMStackOOM();

        try {
            oom.stackLeak();
        } catch (Throwable t) {
            log.error("Stack length:{}", oom.stackLen);
            throw t;
        }
    }
}
