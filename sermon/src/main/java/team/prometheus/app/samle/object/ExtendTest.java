package team.prometheus.app.samle.object;

/**
 * Created on 2016/8/10
 * [Function]
 *
 * @author zhangshuai
 * @version 1.0.0
 * @since 1.0.0
 */
public class ExtendTest {

    public static void main(String[] args) {
        B b = B.getInstance();
        b.invoke();
    }



}
class A {

    public A() {
        System.out.println("Constructed in A...");
    }

    public void out() {
        System.out.println("Printed in A...");
    }

    public void invoke(){
        System.out.println("Invoked in A...");
        out();
    }
}

class B extends A {

    private B() {
        System.out.println("Constructed in B...");
    }

    public void out() {
        System.out.println("Printed in B...");
    }

    public static B getInstance() {
        return new B();
    }
}
