package team.prometheus.app.samle.collctions;

import java.util.LinkedList;

/**
 * Created on 2016/6/27
 * [Function]
 *
 * @author zhangshuai
 * @version 1.0.0
 * @since 1.0.0
 */
public class LinkedListDemo {

    public static void main (String[] args) {
        LinkedList<String> linkedList  = new LinkedList<>();

        linkedList.add("A");
        linkedList.add(2, "B");

        System.out.println(linkedList);
    }
}
