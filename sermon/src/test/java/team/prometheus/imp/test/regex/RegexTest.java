package team.prometheus.imp.test.regex;

import lombok.extern.slf4j.Slf4j;
import team.prometheus.app.sample.annotation.Test;

/**
 * Created on 16/6/15
 * [Function]
 *
 * @author edwin
 * @version 1.1.0
 * @since 1.1.0
 */
@Slf4j
public class RegexTest {

    @Test
    public void Basic(){
        log.info(Boolean.toString("abc".matches("a")));
    }
}
