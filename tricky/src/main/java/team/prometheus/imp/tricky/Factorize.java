package team.prometheus.imp.tricky;

import com.google.common.base.Preconditions;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created on 16/6/16
 * [Function]
 * 对于给定的数因式分解
 * @author edwin
 * @version 1.1.0
 * @since 1.1.0
 */
public class Factorize {

    public static void main(String[] args) {

        while (true) {
            Scanner s = new Scanner(System.in);

            System.out.print("\nPlease input a num:");

            int num = s.nextInt();

            System.out.print("num = ");

            calculate(num);
        }
    }

    private static void calculate(int num) {

        Preconditions.checkArgument(num>0, new RuntimeException("must bigger than 1"));

        if (num <= 2)  System.out.println(num);

        final int MIN = 2; // use to strore the biggest
        List<Integer> list = new ArrayList<>(1);
        list.add(MIN);

        int counter = 1;
        int temp = num;
        boolean newCounter = true;

        if (temp%MIN == 0) {
            System.out.print(MIN);
            temp = temp / MIN;
            if (!list.contains(temp)) {
                System.out.print("*");
            }
        }

        while (counter < temp) {

            if (newCounter) {
                for (Integer x : list) {
                    if (temp % x == 0) {
                        System.out.print(" " + x);
                        list.add(x);
                        temp = temp /x;
                        if (list.contains(temp)){
                            System.out.println(" " + x);
                            return;
                        } else {
                            System.out.print(" *");
                            break;
                        }
                    }
                }
                newCounter = false;
            }


            counter += 2;
            if (temp%counter == 0) {
                System.out.print(" " + counter);
                list.add(counter);
                temp = temp / counter;
                if (temp != 1) {
                    newCounter = true;
                    System.out.print(" *");
                } else {
                    break;
                }
            }
        }

    }
}
