package team.prometheus.imp.tricky;

import com.google.common.base.Preconditions;
import com.sun.javaws.exceptions.InvalidArgumentException;
import team.prometheus.imp.utils.Printer;
import team.prometheus.imp.utils.Timer;

import java.util.Arrays;

/**
 * Created on 16/6/9
 * [场景]
 * 有一对兔子, 从第三个月开始每个月生一对兔子,
 * 小兔子长到三个月后每个月又生一对兔子,
 * 假如所有兔子都不死,
 * 问: 每个月兔子的总数?
 * [关键]
 * 第三个月生兔子,
 * 意味着n-1个月(n>2)的兔子在第n+1个月都会生兔子!!!
 * 而这恰恰是n+1个月的增量,因为其它的都是小兔子.
 *
 * @author edwin
 * @version 1.1.0
 * @since 1.1.0
 */
public class Rabbits {

    public static void main(String[] args){
        Printer.println("* 有一对兔子, 从第三个月开始每个月生一对兔子,\n" +
                "* 小兔子长到三个月后每个月又生一对兔子,\n" +
                "* 假如所有兔子都不死,\n" +
                "* 问: 每个月兔子的总数?");
        Printer.repeatedPrint("--");

        // let's assume 2 years
        int month = 24;
        // calculate it
        Timer.startTimer(Rabbits.class.getName());
        Printer.println("答案是:", calculate(month));
        Timer.stopTimer(Rabbits.class.getName());

        Printer.repeatedPrint("--");
        Printer.println(Rabbits.class.getName(), " : ",Timer.getTime(Rabbits.class.getName()));
    }

    private static int calculate(int month) {
        // 最初就一只
        int res = 1;
        Preconditions.checkNotNull(month, new InvalidArgumentException(new String[]{"NPE!!"}));
        Preconditions.checkArgument(month>0, new InvalidArgumentException(new String[]{"Please give me a correct num!"}));

        if (month<3) {
            return res;
        } else {
            int f1 = 1;
            int f2 = 1;

            for (int i=3 ; i<=month ; i++) {
                res = f1 + f2;
                f1 = f2;
                f2 = res;
                Printer.println(">>logging calculation<<At month ", i, " is ", res);
            }
        }

        return res;

    }
}
