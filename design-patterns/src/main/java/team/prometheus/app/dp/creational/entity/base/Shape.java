package team.prometheus.app.dp.creational.entity.base;

/**
 * Created on 2016/5/30
 * Defines a abstract method named draw()
 *
 * @author zhangshuai
 * @version 1.0.0
 * @since 1.0.0
 */
public interface Shape {
    /**
     * subclass has to implement this method
     */
    void draw();
}
