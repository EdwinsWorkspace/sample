package team.prometheus.app.dp.creational.simpleFactoryPattern.test;

import team.prometheus.app.dp.creational.entity.base.Shape;
import team.prometheus.app.dp.creational.simpleFactoryPattern.factory.MethodFactory;
import team.prometheus.app.dp.creational.simpleFactoryPattern.factory.SimpleFactory;
import team.prometheus.app.dp.utils.Printer;

/**
 * Created on 2016/5/30
 * [Function]
 * This is a basic factory design pattern demo.
 * It displays basic usage of this pattern.
 *
 * @author zhangshuai
 * @version 1.0.0
 * @since 1.0.0
 */
public class SimpleFactoryTest {

    public static void main(String[] args) {
        Shape shape;

        Printer.repeatedPrint("--");
        Printer.println("Here is the basic factory pattern presentation.");
        Printer.repeatedPrint("--");

        draw(SimpleFactory.ShapeType.CIRCLE);
        Printer.println("switching painter...");
        draw(SimpleFactory.ShapeType.RECTANGLE);

        Printer.repeatedPrint("--");
        Printer.println("Here is the multi-method factory pattern presentation.");
        Printer.repeatedPrint("--");

        shape = MethodFactory.produceCircle();
        shape.draw();
        Printer.println("switching painter...");
        shape = MethodFactory.produceSquare();
        shape.draw();
    }

    public static void draw(SimpleFactory.ShapeType type) {
        SimpleFactory.getShape(type).draw();
    }
}
