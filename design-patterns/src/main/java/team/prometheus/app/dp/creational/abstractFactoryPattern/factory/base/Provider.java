package team.prometheus.app.dp.creational.abstractFactoryPattern.factory.base;

import team.prometheus.app.dp.creational.entity.base.Shape;

/**
 * Created on 2016/5/31
 * [Function]
 * Defines common provider method
 * [Motivation]
 * You just need to create a new class which implements this IF.
 * Also your factory can implement other IF like this
 * which makes it flexible and scalable.
 *
 * @author zhangshuai
 * @version 1.0.0
 * @since 1.0.0
 */
public interface Provider {

    Shape provideShape();
}
