package team.prometheus.app.dp.utils.timer;

import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created on 16/6/13
 * [Function]
 *
 * @author edwin
 * @version 1.1.0
 * @since 1.1.0
 */
@Slf4j
public class TimerHelper {

    /**
     *
     */
    private static final ThreadLocal<ConcurrentMap<TimerKey, Long>> timeMap = new ThreadLocal<>();

    private static final ThreadLocal<ConcurrentMap<TimerKey, Status>> statusMap = new ThreadLocal<>();

    private static final ThreadLocal<ConcurrentMap<TimerKey, AtomicInteger>> timerCountMap = new ThreadLocal<>();

    /**
     * Start timers by key
     * @param timerKeys keys you want to start
     */
    public static void startTimer(@NotNull TimerKey... timerKeys) {
        startTimer(false, timerKeys);
    }

    /**
     * Start async timer by key
     * @param countAsync
     * @param timerKeys
     */
    public static void startTimer(@NotNull boolean countAsync, TimerKey... timerKeys) {
        final long now = System.currentTimeMillis();
        for (TimerKey key : timerKeys) {
            final Status status = getStatus(key);

            if (status == Status.STOPPED) {
                getTimeMap().put(key, getTimeInternal(key) * -1 + now);
                getStatusMap().put(key, Status.STARTED);
            }

            if (countAsync || status == Status.STARTED) incrementTimerCounter(key);
        }
    }

    /**
     * Pause timers by key
     * @param timerKeys keys you want to pause
     */
    public static void pauseTimer(@NotNull TimerKey... timerKeys) {
        final long now = System.currentTimeMillis();
        for (TimerKey key : timerKeys) {
            if (getStatus(key) == Status.STARTED) {
                getTimeMap().put(key, now - getTimeInternal(key));
                getStatusMap().put(key, Status.PAUSED);
            }
        }
    }

    /**
     * Resume timers by key
     * @param timerKeys keys you want to resume
     */
    public static void resumeTimer(@NotNull TimerKey... timerKeys) {
        final long now = System.currentTimeMillis();
        for (TimerKey key : timerKeys) {
            if (getStatus(key) == Status.PAUSED) {
                getTimeMap().put(key, getTimeInternal(key) * -1 + now);
                getStatusMap().put(key, Status.STARTED);
            }
        }
    }

    /**
     * Stop timers by key
     * @param timerKeys keys you want to stop
     */
    public static void stopTimer(@NotNull TimerKey... timerKeys) {
        stopTimer(false, timerKeys);
    }

    private static void stopTimer(boolean countAsync, @NotNull TimerKey[] timerKeys) {
        final long now = System.currentTimeMillis();
        for (TimerKey key : timerKeys) {
            if (getStatus(key) == Status.STARTED) {
                if (countAsync) {
                    final int runningCount = decrementTimerCounter(key);
                    if (runningCount != 0) {
                        return; // still have other running timer
                    }

                }
                getTimeMap().put(key, now - getTimeInternal(key)); // calculate time
                getStatusMap().put(key, Status.STOPPED); // change status
                getTimerCountMap().remove(key); // remove counter
            }

        }
    }

    /**
     * Get total consumed time by key
     * @param key
     * @return
     */
    public static long getTime(@NotNull TimerKey key) {
        if (getStatus(key) != Status.STOPPED) {
            log.warn("Timer {} hasn't been stopped yet.", key);
            return -1L;
        }
        return getTimeInternal(key);
    }

    /**
     *
     */
    public void clear() {
        timeMap.remove();
        statusMap.remove();
        timerCountMap.remove();
    }

    private static int incrementTimerCounter(@NotNull TimerKey key) {
        return getTimeCounter(key).incrementAndGet();
    }

    private static int decrementTimerCounter(@NotNull TimerKey key) {
        return getTimeCounter(key).decrementAndGet();
    }

    private static long getTimeInternal(@NotNull TimerKey key) {
        return getTimeMap().get(key) == null ?
                0L : getTimeMap().get(key);
    }

    private static Status getStatus(@NotNull TimerKey key) {
        return getStatusMap().get(key) == null ?
                Status.STOPPED : getStatusMap().get(key);
    }

    private static Map<TimerKey, Status> getStatusMap() {
        ConcurrentMap<TimerKey, Status> map = statusMap.get();
        if (map == null) {
            map = new ConcurrentHashMap<>();
            statusMap.set(map);
        }
        return map;
    }

    private static Map<TimerKey, Long> getTimeMap() {
        ConcurrentMap<TimerKey, Long> map = timeMap.get();
        if (map == null) {
            map = new ConcurrentHashMap<>();
            timeMap.set(map);
        }
        return map;
    }

    private static AtomicInteger getTimeCounter(@NotNull TimerKey key) {
        final Map<TimerKey, AtomicInteger> countMap = getTimerCountMap();
        AtomicInteger count = countMap.get(key);

        if (count == null) {
            count = new AtomicInteger();
            countMap.put(key, count);
        }
        return count;
    }

    private static Map<TimerKey, AtomicInteger> getTimerCountMap() {
        ConcurrentMap<TimerKey, AtomicInteger> map = timerCountMap.get();
        if (map == null) {
            map = new ConcurrentHashMap<>();
            timerCountMap.set(map);
        }
        return map;
    }

    private enum Status {
        STARTED,
        PAUSED,
        STOPPED
    }

}
