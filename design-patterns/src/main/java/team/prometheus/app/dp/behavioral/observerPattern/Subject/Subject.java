package team.prometheus.app.dp.behavioral.observerPattern.Subject;

import team.prometheus.app.dp.behavioral.observerPattern.observer.Observer;

/**
 * Created on 16/6/1
 * [Function]
 *
 * @author edwin
 * @version 1.1.0
 * @since 1.1.0
 */
public interface Subject {

    void add(Observer observer);

    void remove(Observer observer);

    void notifyObservers();

    void operation();
}
