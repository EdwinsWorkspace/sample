package team.prometheus.app.dp.behavioral.observerPattern.observer;

/**
 * Created on 16/6/1
 * [Function]
 *
 * @author edwin
 * @version 1.1.0
 * @since 1.1.0
 */
public interface Observer {

    /**
     * Observer will execute this method once it's notified.
     */
    void update();
}
