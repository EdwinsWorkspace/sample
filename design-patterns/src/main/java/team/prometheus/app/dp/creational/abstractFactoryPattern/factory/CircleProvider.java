package team.prometheus.app.dp.creational.abstractFactoryPattern.factory;

import team.prometheus.app.dp.creational.abstractFactoryPattern.factory.base.Provider;
import team.prometheus.app.dp.creational.entity.Circle;
import team.prometheus.app.dp.creational.entity.base.Shape;

/**
 * Created on 2016/5/31
 * [Function]
 *
 * @author zhangshuai
 * @version 1.0.0
 * @since 1.0.0
 */
public class CircleProvider implements Provider {
    @Override public Shape provideShape() {
        return new Circle();
    }

}
