package team.prometheus.app.dp.structural.adaptor.jdbc;

import lombok.extern.slf4j.Slf4j;

/**
 * Created on 16/6/19
 * [Function]
 *
 * @author edwin
 * @version 1.1.0
 * @since 1.1.0
 */
@Slf4j
public class OracleJDBC implements JDBC {
    @Override
    public void connect(String host, String port, String name, String pwd) {
        log.info("Connecting {}:{}?username={}&password={} by OracleJDBC",
                host,
                port,
                name,
                pwd
        );
    }
}
