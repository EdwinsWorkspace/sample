package team.prometheus.app.dp.behavioral.observerPattern.observer.impl;

import team.prometheus.app.dp.behavioral.observerPattern.observer.Observer;
import team.prometheus.app.dp.utils.Printer;

/**
 * Created on 16/6/1
 * [Function]
 *
 * @author edwin
 * @version 1.1.0
 * @since 1.1.0
 */
public class HanMeimei implements Observer {
    @Override
    public void update() {
        Printer.println("Han: Thanks! Love you ~");
    }
}
