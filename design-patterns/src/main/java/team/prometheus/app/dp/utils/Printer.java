package team.prometheus.app.dp.utils;

/**
 * Created on 2016/5/19
 * [Function]
 *
 * @author zhangshuai
 * @version 1.0.0
 * @since 1.0.0
 */
public class Printer {
    private final static int REPEATE_NUM = 50;

    public static void println(Object... params) {
        for (Object p : params) {
            System.out.print(p);
        }
        System.out.println();
    }

    public  static void repeatedPrint(String original){
        repeatedPrint(original,REPEATE_NUM);
    }

    public  static void repeatedPrint(String original, int num){
        println(repeat(original,num));
    }

    public static String repeat(String original, int num) {
        return new String(new char[num]).replace("\0",original);
    }
}
