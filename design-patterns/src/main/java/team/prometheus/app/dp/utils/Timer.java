package team.prometheus.app.dp.utils;

import com.google.common.base.Preconditions;
import org.apache.commons.lang3.time.DateUtils;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * Created on 16/6/9
 * [Function]
 *
 * @author edwin
 * @version 1.1.0
 * @since 1.1.0
 */
public class Timer {

    private final static ConcurrentMap<String, Time> timer;

    /**
     *s
     */
    private Timer(){}

    static {
        // Mostly just one is enough
        timer = new ConcurrentHashMap<>(1);
    }

    /**
     *
     * @param timeKey
     */
    public static void startTimer(String timeKey) {
        Time t = timer.get(timeKey);
        if (t == null) {
            t = new Time();
            timer.put(timeKey, t);
        }
        {
            Preconditions.checkArgument(t.startable, new RuntimeException());

            t.startTime = System.currentTimeMillis();
            t.stopTime = -1;
            t.timeAmount = -1;
            t.startable = false;
        }
    }

    /**
     *
     * @param timeKey
     */
    public static void stopTimer(String timeKey) {
        Time t = timer.get(timeKey);
        Preconditions.checkNotNull(t, new RuntimeException());
        {
            Preconditions.checkArgument(!t.startable, new RuntimeException());

            t.stopTime = System.currentTimeMillis();
            t.timeAmount = t.stopTime - t.startTime;
            t.timeAmount = t.timeAmount==0? ++t.timeAmount : t.timeAmount;

            StringBuffer sb = new StringBuffer();
            sb.append("tocaltime:").append(t.timeAmount).append("\n");
            sb.append("start:").append(t.startTime).append("\n");
            sb.append("stop:").append(t.stopTime).append("\n");
            Preconditions.checkArgument(t.timeAmount>0, sb.toString());
            t.startable = true;
        }
    }

    public static long getTime(String timeKey) {
        Time t = timer.get(timeKey);
        Preconditions.checkNotNull(t, new RuntimeException());
        synchronized (t) {
            if (t.startable) {
                return t.timeAmount;
            } else {
                long temp = System.currentTimeMillis();
                return temp - t.startTime;
            }
        }
    }

    /**
     *
     */
    private static class Time{
        long startTime;
        long stopTime;
        /**
         * true means startable,
         * false means stopable
         */
        boolean startable = true;

        long timeAmount;
    }
}
