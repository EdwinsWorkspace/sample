package team.prometheus.app.dp.creational.builder.base;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 2016/6/1
 * [Function]
 *
 * @author zhangshuai
 * @version 1.0.0
 * @since 1.0.0
 */
public class Product {
    private List<String> parts;

    public Product() {
        parts = new ArrayList<>();
    }

    public void add (String part) {
        parts.add(part);
    }

    @Override public String toString() {
        return "Product{" +
            "parts=" + parts +
            '}';
    }
}
