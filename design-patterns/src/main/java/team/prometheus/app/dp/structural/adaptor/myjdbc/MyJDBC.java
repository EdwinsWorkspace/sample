package team.prometheus.app.dp.structural.adaptor.myjdbc;

import lombok.extern.slf4j.Slf4j;

/**
 * Created on 16/6/19
 * [Function]
 *
 * @author edwin
 * @version 1.1.0
 * @since 1.1.0
 */
@Slf4j
public class MyJDBC {

    public void connect(MyConfig config) {
        log.info("Connecting {}:{}?username={}&password={} by MyJDBC",
                config.getHost(),
                config.getPort(),
                config.getName(),
                config.getPwd()
                );
    }
}
