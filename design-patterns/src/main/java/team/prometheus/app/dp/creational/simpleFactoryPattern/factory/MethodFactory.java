package team.prometheus.app.dp.creational.simpleFactoryPattern.factory;

import team.prometheus.app.dp.creational.entity.Circle;
import team.prometheus.app.dp.creational.entity.Rectangle;
import team.prometheus.app.dp.creational.entity.Square;
import team.prometheus.app.dp.creational.entity.base.Shape;

/**
 * Created on 2016/6/1
 * [Function]
 * Uses different method to produce different products.
 * [Motivation]
 * This avoid passing product ID directly.
 * [Flaws]
 * Still need to modify this class once changes happen.
 *
 * @author zhangshuai
 * @version 1.0.0
 * @since 1.0.0
 */
public class MethodFactory {

    public static Shape produceCircle(){
        return new Circle();
    }

    public static Shape produceRectangle(){
        return new Rectangle();
    }

    public static Shape produceSquare(){
        return new Square();
    }
}
