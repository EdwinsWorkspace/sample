package team.prometheus.app.dp.behavioral.strategyPattern.impl;

import team.prometheus.app.dp.behavioral.strategyPattern.base.Strategy;
import team.prometheus.app.dp.utils.Printer;

/**
 * Created on 16/6/1
 * [Function]
 *
 * @author edwin
 * @version 1.1.0
 * @since 1.1.0
 */
public class StrategySubstract implements Strategy{

    @Override
    public int doOperation(int num1, int num2) {
        Printer.println("Strategy B ...");
        return num1 - num2;
    }
}
