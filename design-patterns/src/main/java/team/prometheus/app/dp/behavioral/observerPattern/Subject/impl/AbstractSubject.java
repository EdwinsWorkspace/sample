package team.prometheus.app.dp.behavioral.observerPattern.Subject.impl;

import team.prometheus.app.dp.behavioral.observerPattern.Subject.Subject;
import team.prometheus.app.dp.behavioral.observerPattern.observer.Observer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 16/6/1
 * [Function]
 *
 * @author edwin
 * @version 1.1.0
 * @since 1.1.0
 */
public abstract class AbstractSubject implements Subject {

    private static final int INIT_OBSERVER_NUM = 10;

    private List<Observer> observers = new ArrayList<>(INIT_OBSERVER_NUM);

    @Override
    public void add(Observer observer) {
        observers.add(observer);
    }

    @Override
    public void remove(Observer observer) {
        observers.remove(observer);
    }

    @Override
    public void notifyObservers() {
//        observers.forEach((observer) -> observer.update());
        observers.parallelStream().forEach((observer) -> observer.update());
    }

}
