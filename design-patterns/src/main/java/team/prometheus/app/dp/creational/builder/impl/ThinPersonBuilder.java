package team.prometheus.app.dp.creational.builder.impl;

import team.prometheus.app.dp.creational.builder.base.Builder;
import team.prometheus.app.dp.creational.builder.base.Product;
import team.prometheus.app.dp.utils.Printer;

/**
 * Created on 2016/6/1
 * [Function]
 *
 * @author zhangshuai
 * @version 1.0.0
 * @since 1.0.0
 */
public class ThinPersonBuilder implements Builder {

    private Product product = new Product();

    @Override public void buildA() {
        product.add("Thin head");
        Printer.println("## Thin head is built");
    }

    @Override public void buildB() {
        product.add("Thin body");
        Printer.println("## Thin body is built");
    }

    @Override public void buildC() {
        product.add("Thin things");
        Printer.println("## Thin things is built");
    }

    @Override public Product getResult() {
        return product;
    }
}
