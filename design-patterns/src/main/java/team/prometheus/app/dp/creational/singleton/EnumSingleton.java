package team.prometheus.app.dp.creational.singleton;

import team.prometheus.app.dp.utils.Printer;

/**
 * Created on 2016/6/1
 * [Function]
 * concurrent safe // Good
 * easy implement // Good
 * you need java 1.5+
 *
 * @author zhangshuai
 * @version 1.0.0
 * @since 1.0.0
 */
public enum  EnumSingleton {

    /**
     * single instance
     */
    INSTANCE;

    /**
     * You can put some function codes here.
     * But it's not safe for concurrent.
     */
    public void someFunctionalMethod() {
        Printer.println("SomeFunctionalMethod is invoked in EnumSingleton.");
        Printer.println("This method is concurrent safe");
        Printer.println("Enum is a new feature since java 1.5.");
    }
}
