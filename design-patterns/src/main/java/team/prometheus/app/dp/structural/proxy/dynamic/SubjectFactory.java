package team.prometheus.app.dp.structural.proxy.dynamic;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

/**
 * Created on 2016/7/22
 * [Function]
 *
 * @author zhangshuai
 * @version 1.0.0
 * @since 1.0.0
 */
public class SubjectFactory {

    private final static Subject INSTANCE;

    static {
        // init subject & handler
        Subject subject = new ConcreteSubject();
        InvocationHandler handler = new ProxyHandler(subject);

        // use classloader new a proxy instance
        INSTANCE = (Subject) Proxy.newProxyInstance(subject.getClass().getClassLoader(),
            subject.getClass().getInterfaces(), handler);
    }

    public static Subject getInstance() {
        return INSTANCE;
    }
}
