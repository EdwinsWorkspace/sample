package team.prometheus.app.dp.creational.prototype;

import java.io.*;

/**
 * Created on 2016/6/6
 * [Function]
 *
 * [Note]
 * Cloneable is an empty IF.
 *
 * @author zhangshuai
 * @version 1.0.0
 * @since 1.0.0
 */
public class ProtoType implements Cloneable, Serializable {

    /**
     * shallow copy
     * @return
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        ProtoType proto = (ProtoType) super.clone();
        return proto;
    }

    public Object deepClone() throws IOException, ClassNotFoundException {

        /**
         * write object
         */
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(bos);
        oos.writeObject(this);

        ByteArrayInputStream bis = new ByteArrayInputStream(bos.toByteArray());
        ObjectInputStream ois = new ObjectInputStream(bis);
        return ois.readObject();
    }
}
