package team.prometheus.app.dp.creational.singleton;

import team.prometheus.app.dp.utils.Printer;

/**
 * Created on 2016/6/1
 * [Function]
 * Provides singleton instance.
 *
 * Concurrent safe as mechanism of ClassLoader // Good
 *
 * [Motivation]
 * To reduce resources usage.
 *
 * [Flaws]
 * Instance is initialized once class is loaded.
 *
 * @author zhangshuai
 * @version 1.0.0
 * @since 1.0.0
 */
public class SimpleSingleton {

    /**
     * It need to create instance by itself.
     *
     * [Note]
     * 1. Added "final" here is much better
     * 2. Also you can just declare it here, and initial it in a static block.
     */
    private static final SimpleSingleton instance = new SimpleSingleton();

    /**
     * It need hide its constructor to make sure its singleton.
     */
    private SimpleSingleton(){
        Printer.println("##[INFO] invoked constructor of SimpleSingleton.");
    }

    /**
     * It must expose itself.
     * @return singleton instance
     */
    public static SimpleSingleton getInstance() {
        Printer.println("##[INFO] invoked getInstance(),");
        return instance;
    }

    /**
     * You can put some function codes here.
     * But it's not safe for concurrent.
     */
    public void someFunctionalMethod() {
        Printer.println("SomeFunctionalMethod is invoked in SimpleSingleton.");
        Printer.println("This method is concurrent safe.");
        Printer.println("Instance is initialized once class is loaded.");
    }

}
