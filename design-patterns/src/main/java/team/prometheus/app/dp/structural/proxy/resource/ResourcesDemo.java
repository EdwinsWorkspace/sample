package team.prometheus.app.dp.structural.proxy.resource;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.Field;
import java.util.ArrayList;

/**
 * Created on 16/6/12
 * [Function]
 *
 * @author edwin
 * @version 1.1.0
 * @since 1.1.0
 */
@Slf4j
public class ResourcesDemo implements Loader<Picture>{

    @Getter @Setter
    Picture picture;

    public static void main(String[] args) {

        ResourcesDemo resources = new ResourcesDemo();

        Thread thread = new Thread(resources);

        thread.start();

        Class clazz = resources.getClass();

        while(true) {
            try {
                Field field = clazz.getDeclaredField("picture");
                Picture picture = (Picture) field.get(resources);

                if (picture == null) break;

                int status = picture.getParts().intValue();
                log.info(String.valueOf(status * 10) + "%");

                if (status >= 10) return;

            } catch (NoSuchFieldException | IllegalAccessException e) {
                log.error(e.getMessage());
            }
        }

    }

    @Override
    public void before() {
        log.info("before");
    }

    @Override
    public void after() {
        log.info("after");
    }

    @Override
    public String load() {

        picture = new Picture();
        try {
            // this is gonna take a while
            // you need to check its status
            picture.readFile();
        } catch (InterruptedException e) {
            log.error(e.getMessage());
        }
        return null;
    }

    @Override
    public void run() {
        before();
        load();
        after();
    }
}
