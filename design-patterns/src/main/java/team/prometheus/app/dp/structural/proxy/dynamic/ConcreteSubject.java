package team.prometheus.app.dp.structural.proxy.dynamic;

import lombok.extern.slf4j.Slf4j;

/**
 * Created on 16/6/13
 * [Function]
 *
 * @author edwin
 * @version 1.1.0
 * @since 1.1.0
 */
@Slf4j
public class ConcreteSubject implements Subject {

    @Override
    public int methodA(int num) {
        log.info("num: {}", num);
        return num;
    }

    @Override
    public String methodB(String msg) {
        log.info("Message: {}", msg);
        return msg;
    }
}
