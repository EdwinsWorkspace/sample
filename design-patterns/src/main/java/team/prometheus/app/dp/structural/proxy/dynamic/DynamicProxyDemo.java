package team.prometheus.app.dp.structural.proxy.dynamic;

import lombok.extern.slf4j.Slf4j;
import team.prometheus.app.dp.utils.timer.TimerHelper;
import team.prometheus.app.dp.utils.timer.TimerKey;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

/**
 * Created on 16/6/13
 * [Function]
 *
 * @author edwin
 * @version 1.1.0
 * @since 1.1.0
 */
@Slf4j
public class DynamicProxyDemo {

    public static void main(String[] args){

        Subject target = SubjectFactory.getInstance();

        target.methodA(50);

        target.methodB("Hello Proxy!!!");

    }
}
