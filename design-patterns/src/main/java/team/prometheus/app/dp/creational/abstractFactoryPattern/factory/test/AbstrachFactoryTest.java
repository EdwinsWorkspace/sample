package team.prometheus.app.dp.creational.abstractFactoryPattern.factory.test;

import team.prometheus.app.dp.creational.abstractFactoryPattern.factory.CircleProvider;
import team.prometheus.app.dp.creational.abstractFactoryPattern.factory.SquareProvider;
import team.prometheus.app.dp.creational.abstractFactoryPattern.factory.base.Provider;
import team.prometheus.app.dp.utils.Printer;

/**
 * Created on 2016/5/31
 * [Function]
 * This is a basic abstract factory design pattern demo.
 *
 * @author zhangshuai
 * @version 1.0.0
 * @since 1.0.0
 */
public class AbstrachFactoryTest {

    public static void main(String[] args) {

        Provider provider = new CircleProvider();
        provider.provideShape().draw();

        Printer.println("switching painter...");

        provider = new SquareProvider();
        provider.provideShape().draw();
    }
}
