package team.prometheus.app.dp.creational.builder.impl;

import team.prometheus.app.dp.creational.builder.base.Builder;
import team.prometheus.app.dp.creational.builder.base.Product;
import team.prometheus.app.dp.utils.Printer;

/**
 * Created on 2016/6/1
 * [Function]
 *
 * @author zhangshuai
 * @version 1.0.0
 * @since 1.0.0
 */
public class FatPersonBuilder implements Builder {

    private Product product = new Product();

    @Override public void buildA() {
        product.add("Fat head");
        Printer.println("## Fat head is built");
    }

    @Override public void buildB() {
        product.add("Fat body");
        Printer.println("## Fat body is built");
    }

    @Override public void buildC() {
        product.add("Fat things");
        Printer.println("## Fat things is built");
    }

    @Override public Product getResult() {
        return product;
    }
}
