package team.prometheus.app.dp.creational.entity;

import team.prometheus.app.dp.creational.entity.base.Shape;
import team.prometheus.app.dp.utils.Printer;

/**
 * Created on 2016/5/30
 * [Function]
 *
 * @author zhangshuai
 * @version 1.0.0
 * @since 1.0.0
 */
public class Square implements Shape {
    @Override public void draw() {
        Printer.repeatedPrint("--");
        Printer.println(Square.class.getName());
        Printer.repeatedPrint("--");
    }
}
