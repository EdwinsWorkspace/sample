package team.prometheus.app.dp.structural.proxy.permission;


import lombok.Getter;
import lombok.Setter;

/**
 * Created on 16/6/12
 * [Function]
 *
 * @author edwin
 * @version 1.1.0
 * @since 1.1.0
 */
@Setter
@Getter
public class MyForum implements Forum {

    private String name;

    @Override
    public void createBlog() {
        System.out.println("calling " + this.name);
    }
}
