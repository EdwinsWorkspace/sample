package team.prometheus.app.dp.structural.adaptor.magicadaptor;

import lombok.AllArgsConstructor;
import lombok.Setter;
import team.prometheus.app.dp.structural.adaptor.jdbc.JDBC;
import team.prometheus.app.dp.structural.adaptor.myjdbc.MyConfig;
import team.prometheus.app.dp.structural.adaptor.myjdbc.MyJDBC;

/**
 * Created on 16/6/19
 * [Function]
 *
 * @author edwin
 * @version 1.1.0
 * @since 1.1.0
 */
@AllArgsConstructor
public class MyJDBCAdaptor implements JDBC {

    private MyJDBC myJDBC;

    @Override
    public void connect(String host, String port, String name, String pwd) {
        MyConfig config = new MyConfig(host, port, name, pwd);
        myJDBC.connect(config);
    }
}
