package team.prometheus.app.dp.behavioral.observerPattern;

import team.prometheus.app.dp.behavioral.observerPattern.Subject.impl.Boss;
import team.prometheus.app.dp.behavioral.observerPattern.observer.impl.Alex;
import team.prometheus.app.dp.behavioral.observerPattern.observer.impl.HanMeimei;

/**
 * Created on 16/6/1
 * [Function]
 *
 * @author edwin
 * @version 1.1.0
 * @since 1.1.0
 */
public class ObeserverDemo {

    public static void main(String[] args) {
        Boss boss = new Boss();

        boss.add(new Alex());
        boss.add(new HanMeimei());

        boss.operation();
    }
}
