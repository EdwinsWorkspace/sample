package team.prometheus.app.dp.structural.proxy.dynamic;

/**
 * Created on 16/6/13
 * [Function]
 *
 *
 * @author edwin
 * @version 1.1.0
 * @since 1.1.0
 */
public interface Subject {

    int methodA(int num);

    String methodB(String msg);
}
