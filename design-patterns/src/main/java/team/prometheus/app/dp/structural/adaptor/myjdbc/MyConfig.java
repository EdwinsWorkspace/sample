package team.prometheus.app.dp.structural.adaptor.myjdbc;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * Created on 16/6/19
 * [Function]
 *
 * @author edwin
 * @version 1.1.0
 * @since 1.1.0
 */
@Getter @Setter
@AllArgsConstructor
public class MyConfig {

    private String host;
    private String port;
    private String name;
    private String pwd;
}
