package team.prometheus.app.dp.creational.singleton.test;

import team.prometheus.app.dp.creational.singleton.*;
import team.prometheus.app.dp.utils.Printer;

/**
 * Created on 2016/6/1
 * [Function]
 *
 * @author zhangshuai
 * @version 1.0.0
 * @since 1.0.0
 */
public class SingletonTest {

    public static void main(String[] args){
        // simple singleton
        SimpleSingleton.getInstance().someFunctionalMethod();
        Printer.repeatedPrint("--");

        // lazy singleton
        LazySingleton.getInstance().someFunctionalMethod();
        Printer.repeatedPrint("--");

        // lazy synchronized singleton
        LazySynchronizedSingleton.getInstance().someFunctionalMethod();
        Printer.repeatedPrint("--");

        // lazy synchronized singleton
        StaticNestedClassSingleton.getInstance().someFunctionalMethod();
        Printer.repeatedPrint("--");

        // Enum singleton
        EnumSingleton.INSTANCE.someFunctionalMethod();
        Printer.repeatedPrint("--");

        // Enum singleton
        DoubleCheckedLockingSingleton.getInstance().someFunctionalMethod();
        Printer.repeatedPrint("--");
    }
}
