package team.prometheus.app.dp.creational.simpleFactoryPattern.factory;

import team.prometheus.app.dp.creational.entity.Circle;
import team.prometheus.app.dp.creational.entity.Rectangle;
import team.prometheus.app.dp.creational.entity.Square;
import team.prometheus.app.dp.creational.entity.base.Shape;

/**
 * Created on 2016/5/30
 * [Function]
 * This is a factory class which provides "Shape" by type parameters
 *
 * [Motivation]
 * You need to produce products with similar function.
 * So you create a factory like this.
 * You just need to tell it the ID of your product.
 *
 * [Flaws]
 * The ID might be mistaken when you pass it to factory.
 * And you need to verified this class every time you need a new product.
 *
 * @author zhangshuai
 * @version 1.0.0
 * @since 1.0.0
 */
public class SimpleFactory {

    private SimpleFactory(){}

    public static Shape getShape(ShapeType type) {
        if (type == null) {
            return null;
        }

        switch (type) {
            case CIRCLE:
                return new Circle();
            case RECTANGLE:
                return new Rectangle();
            case SQUARE:
                return new Square();
            default:
                return null;
        }
    }

    public enum ShapeType{
        CIRCLE,
        RECTANGLE,
        SQUARE
    }
}
