package team.prometheus.app.dp.creational.builder.base;

/**
 * Created on 2016/6/1
 * [Function]
 *
 * @author zhangshuai
 * @version 1.0.0
 * @since 1.0.0
 */
public interface Builder {

    void buildA();
    void buildB();
    void buildC();

    Product getResult();
}
