package team.prometheus.app.dp.structural.proxy.permission;

/**
 * Created on 16/6/12
 * [Function]
 *
 * @author edwin
 * @version 1.1.0
 * @since 1.1.0
 */
public interface Forum {

    void createBlog();
}
