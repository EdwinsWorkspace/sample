package team.prometheus.app.dp.behavioral.strategyPattern;

import team.prometheus.app.dp.behavioral.strategyPattern.base.Strategy;
import team.prometheus.app.dp.behavioral.strategyPattern.impl.StrategyAdd;
import team.prometheus.app.dp.utils.Printer;

/**
 * Created by edwin on 16/6/1.
 */
public class StrategyTest {

    public static void main(String[] args) {
        Strategy strategy;
        Context context;

        strategy = new StrategyAdd();
        context = new Context(strategy);
        Printer.println("10+3=?");
        Printer.println(context.execute(10, 3));
    }
}
