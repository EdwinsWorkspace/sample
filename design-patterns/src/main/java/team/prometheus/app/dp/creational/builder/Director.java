package team.prometheus.app.dp.creational.builder;

import team.prometheus.app.dp.creational.builder.base.Builder;

/**
 * Created on 2016/6/1
 * [Function]
 * Director determined how to build a product.
 * Builder just need to know how to build part of product.
 * You can define different builder in this class.
 * Also you can use different director to build different products.
 * Even you can regard them as workers, and create a pool to reduce resources.
 *
 * @author zhangshuai
 * @version 1.0.0
 * @since 1.0.0
 */
public class Director {

    private Builder builder;

    public Director (Builder builder) {
        this.builder = builder;
    }

    /**
     * This method defined how to build a "product"
     */
    public void construct() {
        builder.buildA();
        builder.buildB();
        builder.buildC();
    }
}
