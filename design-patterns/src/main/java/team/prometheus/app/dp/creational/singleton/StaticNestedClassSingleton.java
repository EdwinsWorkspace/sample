package team.prometheus.app.dp.creational.singleton;

import team.prometheus.app.dp.utils.Printer;

/**
 * Created on 2016/6/1
 * [Function]
 * Provides singleton instance.
 *
 * Concurrent safe as mechanism of ClassLoader // Good
 *
 * [Motivation]
 * To reduce resources usage.
 *
 * [Flaws]
 * Instance is initialized only "getInstance" is invoked
 *
 * @author zhangshuai
 * @version 1.0.0
 * @since 1.0.0
 */
public class StaticNestedClassSingleton {

    private String name;

    /**
     * It needs to hide its constructor to make sure its singleton.
     */
    private StaticNestedClassSingleton(){
        Printer.println("##[INFO] invoked constructor of StaticNestedClassSingleton.");
    }

    /**
     * It must expose itself.
     * @return singleton instance
     */
    public static StaticNestedClassSingleton getInstance() {
        Printer.println("##[INFO] invoked getInstance(),");
        return SingletonHolder.instance;
    }

    /**
     * You can put some function codes here.
     * But it's not safe for concurrent.
     */
    public void someFunctionalMethod() {
        Printer.println("SomeFunctionalMethod is invoked in StaticNestedClassSingleton.");
        Printer.println("This method is concurrent safe.");
    }

    /**
     * Static nested class which can initialize instance.
     */
    private static class SingletonHolder{

        private static final StaticNestedClassSingleton instance = new StaticNestedClassSingleton();
    }

}
