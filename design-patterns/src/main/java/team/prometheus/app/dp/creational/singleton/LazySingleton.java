package team.prometheus.app.dp.creational.singleton;

import team.prometheus.app.dp.utils.Printer;

/**
 * Created on 2016/6/1
 * [Function]
 * Instance is only initialized when it's required. // Good
 * [Flaws]
 * More than one instance would be initialized // NG
 * if several threads invoke getInstance at same time
 *
 * @author zhangshuai
 * @version 1.0.0
 * @since 1.0.0
 */
public class LazySingleton{
    /**
     * It needs to create instance by itself.
     */
    private static LazySingleton instance;

    /**
     * It needs hide its constructor to make sure its singleton.
     */
    private LazySingleton(){
        Printer.println("##[INFO] invoked constructor of LazySingleton.");
    }

    /**
     * It must expose itself.
     * @return singleton instance
     */
    public static LazySingleton getInstance() {
        Printer.println("##[INFO] invoked getInstance(),");
        if (instance == null) {
            instance = new LazySingleton();
        }
        return instance;
    }

    /**
     * You can put some function codes here.
     * But it's not safe for concurrent.
     */
    public void someFunctionalMethod() {
        Printer.println("SomeFunctionalMethod is invoked in LazySingleton.");
        Printer.println("This method is not concurrent safe");
        Printer.println("More than one instance would be initialized if several threads invoke getInstance at same time");
    }
}
