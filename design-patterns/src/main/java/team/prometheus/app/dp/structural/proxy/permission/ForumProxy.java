package team.prometheus.app.dp.structural.proxy.permission;

/**
 * Created on 16/6/12
 * [Function]
 * static proxy
 *
 * @author edwin
 * @version 1.1.0
 * @since 1.1.0
 */
public class ForumProxy implements Forum {

    /**
     * it must have a forum object
     */
    private Forum forum;

    /**
     * It need authorization besides forum itself
     */
    private int permission;

    /**
     *
     */
    public ForumProxy(Forum forum, int permission){
        this.forum = forum;
        this.permission = permission;
    }

    @Override
    public void createBlog() {
        // check permission before we execute real action.
        if (permission == Permissions.ADMIN || permission == Permissions.CREATE) {
            forum.createBlog();
        }
    }
}
