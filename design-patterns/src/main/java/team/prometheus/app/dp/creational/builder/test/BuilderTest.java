package team.prometheus.app.dp.creational.builder.test;

import team.prometheus.app.dp.creational.builder.impl.ConcreateBuilder;
import team.prometheus.app.dp.creational.builder.Director;
import team.prometheus.app.dp.creational.builder.base.Builder;
import team.prometheus.app.dp.creational.builder.impl.FatPersonBuilder;
import team.prometheus.app.dp.creational.builder.impl.ThinPersonBuilder;
import team.prometheus.app.dp.utils.Printer;

/**
 * Created on 2016/6/1
 * [Function]
 *
 * @author zhangshuai
 * @version 1.0.0
 * @since 1.0.0
 */
public class BuilderTest {

    public static void main(String[] args) {

        Director director;
        Builder builder;

        Printer.println("Basic builder presentation.");
        Printer.repeatedPrint("--");

        builder = new ConcreateBuilder();
        director = new Director(builder);
        director.construct();
        Printer.println(builder.getResult().toString());

        Printer.repeatedPrint("--");

        builder = new FatPersonBuilder();
        director = new Director(builder);
        director.construct();
        Printer.println(builder.getResult().toString());

        Printer.repeatedPrint("--");

        builder = new ThinPersonBuilder();
        director = new Director(builder);
        director.construct();
        Printer.println(builder.getResult().toString());

        Printer.repeatedPrint("--");

    }

}
