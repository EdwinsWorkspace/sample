package team.prometheus.app.dp.behavioral.strategyPattern;

import team.prometheus.app.dp.behavioral.strategyPattern.base.Strategy;

/**
 * Created on 16/6/1
 * [Function]
 *
 * @author edwin
 * @version 1.1.0
 * @since 1.1.0
 */
public class Context {

    private Strategy strategy;

    public Context(Strategy strategy) {
        this.strategy = strategy;
    }

    public int execute(int num1, int num2) {
        return strategy.doOperation(num1, num2);
    }
}
