package team.prometheus.app.dp.creational.singleton;

import team.prometheus.app.dp.utils.Printer;

/**
 * Created on 2016/6/1
 * [Function]
 *
 * [Flaws]
 * I don't like it.
 *
 * @author zhangshuai
 * @version 1.0.0
 * @since 1.0.0
 */
public class DoubleCheckedLockingSingleton {
    /**
     * It needs to create instance by itself.
     */
    private volatile static DoubleCheckedLockingSingleton instance;

    /**
     * It needs hide its constructor to make sure its singleton.
     */
    private DoubleCheckedLockingSingleton(){
        Printer.println("##[INFO] invoked constructor of DoubleCheckedLockingSingleton.");
    }

    /**
     * It must expose itself.
     * @return singleton instance
     */
    public static DoubleCheckedLockingSingleton getInstance() {
        Printer.println("##[INFO] invoked getInstance(),");
        if (instance == null) {
            synchronized (DoubleCheckedLockingSingleton.class) {
                if (instance == null) {
                    instance = new DoubleCheckedLockingSingleton();
                }
            }
        }
        return instance;
    }

    /**
     * You can put some function codes here.
     * But it's not safe for concurrent.
     */
    public void someFunctionalMethod() {
        Printer.println("SomeFunctionalMethod is invoked in DoubleCheckedLockingSingleton.");
        Printer.println("This method is concurrent safe");
        Printer.println("I don't like it...");
    }
}
