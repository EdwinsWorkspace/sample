package team.prometheus.app.dp.behavioral.observerPattern.Subject.impl;

import team.prometheus.app.dp.utils.Printer;

/**
 * Created on 16/6/1
 * [Function]
 *
 * @author edwin
 * @version 1.1.0
 * @since 1.1.0
 */
public class Boss extends AbstractSubject {

    @Override
    public void operation() {
        Printer.println("Boss: Ate something ...");
        Printer.println("Boss: Drank something ...");
        Printer.println("Boss: Oh, shit! need to pay my employees");

        Printer.repeatedPrint("$");
        notifyObservers();
        Printer.repeatedPrint("$");

        Printer.println("Boss: F...");

    }
}
