package team.prometheus.app.dp.creational.builder.impl;

import team.prometheus.app.dp.creational.builder.base.Builder;
import team.prometheus.app.dp.creational.builder.base.Product;
import team.prometheus.app.dp.utils.Printer;

/**
 * Created on 2016/6/1
 * [Function]
 *
 * @author zhangshuai
 * @version 1.0.0
 * @since 1.0.0
 */
public class ConcreateBuilder implements Builder {

    private Product product = new Product();

    @Override public void buildA() {
        // build A here
        product.add("A");
        Printer.println("## A is built");
    }

    @Override public void buildB() {
        // build B here
        product.add("B");
        Printer.println("## B is built");
    }

    @Override public void buildC() {
        // build C here
        product.add("C");
        Printer.println("## C is built");
    }

    @Override public Product getResult() {
        return product;
    }
}
