package team.prometheus.app.dp.structural.proxy.dynamic;

import lombok.extern.slf4j.Slf4j;
import team.prometheus.app.dp.utils.Timer;
import team.prometheus.app.dp.utils.timer.TimerHelper;
import team.prometheus.app.dp.utils.timer.TimerKey;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.Arrays;

/**
 * Created on 16/6/13
 * [Function]
 *
 * @author edwin
 * @version 1.1.0
 * @since 1.1.0
 */
@Slf4j
public class ProxyHandler implements InvocationHandler{

    /**
     * need a instance
     */
    private Subject subject;

    /**
     * constructor
     */
    public ProxyHandler(Subject subject) {
        log.info("Initial handler, started...");
        this.subject = subject;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        log.info("Started proxy handler...");
//        log.info("---------Subject info---------");
//
//        log.info(">> Invoked subject: {} ", proxy.getClass().getName());
//        log.info("- method: {}", method.getName());
//        log.info("- args: {}", Arrays.toString(args));

        log.info("Before {}", method.getName());

        TimerHelper.startTimer(TimerKey.NORMAL);
        Object o = method.invoke(subject, args); // dynamically invoke methods
        TimerHelper.stopTimer(TimerKey.NORMAL);

        log.info("After {}", method.getName());
        log.info(">>>>TotalTime: {} ms", TimerHelper.getTime(TimerKey.NORMAL));

        return o;
    }
}
