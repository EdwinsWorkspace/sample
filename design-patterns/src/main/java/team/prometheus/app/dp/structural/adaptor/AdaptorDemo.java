package team.prometheus.app.dp.structural.adaptor;

import team.prometheus.app.dp.structural.adaptor.jdbc.JDBC;
import team.prometheus.app.dp.structural.adaptor.jdbc.OracleJDBC;
import team.prometheus.app.dp.structural.adaptor.magicadaptor.MyJDBCAdaptor;
import team.prometheus.app.dp.structural.adaptor.myjdbc.MyJDBC;

/**
 * Created on 16/6/19
 * [Function]
 *
 * @author edwin
 * @version 1.1.0
 * @since 1.1.0
 */
public class AdaptorDemo {
    private static final String HOST = "http://localhost";
    private static final String PORT = "3306";
    private static final String USER = "test";
    private static final String PWD = "123456";

    public static void main(String[] args){

        JDBC jdbc = new MyJDBCAdaptor(new MyJDBC());
        jdbc.connect(HOST, PORT, USER, PWD);

        jdbc = new OracleJDBC();
        jdbc.connect(HOST, PORT, USER, PWD);

    }
}
