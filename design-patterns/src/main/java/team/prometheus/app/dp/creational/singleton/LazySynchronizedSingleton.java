package team.prometheus.app.dp.creational.singleton;

import team.prometheus.app.dp.utils.Printer;

/**
 * Created on 2016/6/1
 * [Function]
 * Instance is only initialized when it's required. // Good
 * Concurrent safe. // Good
 * [Flaws]
 * But it has bad performance. // NG
 * Cause just we need to avoid duplicated initialization
 *
 * @author zhangshuai
 * @version 1.0.0
 * @since 1.0.0
 */
public class LazySynchronizedSingleton {
    /**
     * It needs to create instance by itself.
     */
    private static LazySynchronizedSingleton instance;

    /**
     * It needs to hide its constructor to make sure its singleton.
     */
    private LazySynchronizedSingleton(){
        Printer.println("##[INFO] invoked constructor of LazySynchronizedSingleton.");
    }

    /**
     * It must expose itself.
     * @return singleton instance
     */
    public static synchronized LazySynchronizedSingleton getInstance() {
        Printer.println("##[INFO] invoked getInstance(),");
        if (instance == null) {
            instance = new LazySynchronizedSingleton();
        }
        return instance;
    }

    /**
     * You can put some function codes here.
     * But it's not safe for concurrent.
     */
    public void someFunctionalMethod() {
        Printer.println("SomeFunctionalMethod is invoked in LazySynchronizedSingleton.");
        Printer.println("This method is concurrent safe");
        Printer.println("It has bad performance. Cause just we need to avoid duplicated initialization");
    }
}
