package team.prometheus.app.dp.structural.proxy.resource;

/**
 * Created on 16/6/12
 * [Function]
 *
 * @author edwin
 * @version 1.1.0
 * @since 1.1.0
 */
public interface Loader<T extends Picture> extends Runnable{

    void before();

    void after();

    String load();
}
