package team.prometheus.app.dp.structural.proxy.resource;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created on 16/6/12
 * [Function]
 *
 * @author edwin
 * @version 1.1.0
 * @since 1.1.0
 */
@Slf4j
public class Picture {

    @Getter
    private AtomicInteger parts= new AtomicInteger();

    public AtomicInteger readFile() throws InterruptedException {

        for (int i=0; i<10; i++) {
            Thread.currentThread().wait(100);
            parts.getAndIncrement();
        }

        return parts;
    }
}
