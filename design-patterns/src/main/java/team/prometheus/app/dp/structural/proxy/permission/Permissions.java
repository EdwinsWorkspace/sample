package team.prometheus.app.dp.structural.proxy.permission;

/**
 * Created on 16/6/12
 * [Function]
 *
 * @author edwin
 * @version 1.1.0
 * @since 1.1.0
 */
public class Permissions {

    public static final int READ = 0;

    public static final int WRITE = 1;

    public static final int CREATE = 2;

    public static final int UPDATE = 3;

    public static final int ADMIN = 4;

}
