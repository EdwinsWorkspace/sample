package team.prometheus.app.dp.behavioral.strategyPattern.base;

/**
 * Created on 16/6/1
 * [Function]
 *
 * @author edwin
 * @version 1.1.0
 * @since 1.1.0
 *
 */
public interface Strategy {

    int doOperation(int num1, int num2);
}
