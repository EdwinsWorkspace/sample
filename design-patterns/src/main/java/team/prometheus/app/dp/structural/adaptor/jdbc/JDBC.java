package team.prometheus.app.dp.structural.adaptor.jdbc;

/**
 * Created on 16/6/19
 * [Function]
 *
 * @author edwin
 * @version 1.1.0
 * @since 1.1.0
 */
public interface JDBC {

    void connect(String host, String port, String name, String pwd);
}
